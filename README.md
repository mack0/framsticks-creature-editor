# Framsticks Creature Editor #

This application lets you visually edit [Framsticks](http://www.framsticks.com/) creatures. It is based on [Framsticks SDK](http://www.framsticks.com/sdk).

The editor is released under the Creative Commons [CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/legalcode) license.
		
Contact [Framsticks support](mailto:support@framsticks.com) with questions about this project.