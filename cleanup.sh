#!/bin/bash

# Define the folder to clean
TARGET_DIR="./node_modules"

# List of paths to keep
KEEP_FILES=(
  "$TARGET_DIR/jquery-ui/dist/jquery-ui.min.js"
  "$TARGET_DIR/layout-jquery3/dist/jquery.layout_and_plugins.min.js"
  "$TARGET_DIR/layout-jquery3/source/stable/layout-default.css"
  "$TARGET_DIR/three/build/three.module.min.js"
  "$TARGET_DIR/three/examples/jsm/controls/OrbitControls.js"
  "$TARGET_DIR/three/examples/jsm/controls/TransformControls.js"
  "$TARGET_DIR/file-saver/dist/FileSaver.min.js"
  "$TARGET_DIR/file-saver/dist/FileSaver.min.js.map"
  "$TARGET_DIR/ace-builds/src-min/ace.js"
  "$TARGET_DIR/ace-builds/src-min/theme-tomorrow_night_bright.js"
  "$TARGET_DIR/jquery/dist/jquery.min.js"
)

KEEP_DIRS=(
	"$TARGET_DIR/jquery-ui/dist/themes/ui-darkness"
)

orParents() {
  p="$1"
  while
    echo "$p"
    p=$(dirname "$p")
    [ "$p" != . ]
  do :; done
}

ALL_KEEP_FILES=("${KEEP_FILES[@]}" "${KEEP_DIRS[@]}")
RESULT=""
for path in "${ALL_KEEP_FILES[@]}"; do
  RESULT+="$(orParents "$path") "
done

# Remove duplicates by sorting and using uniq
result_array=($(printf '%s\n' $RESULT | sort -u))

path_patterns=""
for path in "${result_array[@]}"; do
  path_patterns+=" -path $path -o"
done

regex_patterns=""
for path in "${KEEP_DIRS[@]}"; do
  regex_patterns+=" -regex ^$path\(/.*\)? -o"
done
regex_patterns="${regex_patterns% -o}"

# Find and delete everything except the specified paths
find $TARGET_DIR -mindepth 1 \( ! \( $path_patterns $regex_patterns \) \) -exec rm -rf {} +

echo "Cleanup complete!"
