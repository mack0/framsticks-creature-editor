(function() {

  const LIB_JQUERY_UI = { src: "node_modules/jquery-ui/dist/jquery-ui.min.js" };
  const LIB_JQUERY_LAYOUT = { src: "node_modules/layout-jquery3/dist/jquery.layout_and_plugins.min.js" };
  const LIB_THREE_JS = { src: "node_modules/three/build/three.module.min.js", type: "module"};
  const LIB_ORBIT_CONTROLS = { src: "node_modules/three/examples/jsm/controls/OrbitControls.js", type: "module" };
  const LIB_TRANSFORM_CONTROLS = { src: "node_modules/three/examples/jsm/controls/TransformControls.js", type: "module" };
  const LIB_FILE_SAVER = { src: "node_modules/file-saver/dist/FileSaver.min.js" };
  const LIB_FRAMS_SDK = { src: "js/framsticks/frams-sdk.js" };
  const LIB_ACE = { src: "node_modules/ace-builds/src-min/ace.js" };
  const FRED_UTILS_FILEHANDLER = { src: "js/utils/filehandler.js" };
  const FRED_UTILS_EVENTS = { src: "js/utils/events.js" };
  const FRED_UTILS_STRINGHELPER = { src: "js/utils/stringhelper.js" };
  const FRED_FRAMSTICKS_LOGPARSER = { src: "js/framsticks/logparser.js" };
  const FRED_FRAMSTICKS = { src: "js/framsticks/framsticks.js" };
  const FRED_BODYEDITOR_GRID = { src: "js/bodyeditor/grid.js"};
  const FRED_BODYEDITOR_SCENEMOUSEINPUT = { src: "js/bodyeditor/scenemouseinput.js" };
  const FRED_BODYEDITOR_TRANSFORMATIONS = { src: "js/bodyeditor/transformations.js" };
  const FRED_BODYEDITOR_RAYCASTER = { src: "js/bodyeditor/raycaster.js" };
  const FRED_BODYEDITOR_CONTROLLER = { src: "js/bodyeditor/controller.js" };
  const FRED_BODYEDITOR_PROPERTIESEDITPANEL = { src: "js/bodyeditor/propertieseditpanel.js" };
  const FRED_BODYEDITOR_ACTIONMENU = { src: "js/bodyeditor/actionmenu.js" };
  const FRED_BODYEDITOR_JOINTMESHFACTORY = { src: "js/bodyeditor/jointmeshfactory.js" };
  const FRED_BODYEDITOR_PARTMESHFACTORY = { src: "js/bodyeditor/partmeshfactory.js" };
  const FRED_BODYEDITOR_BODYELEMENTEDITTOOLS = { src: "js/bodyeditor/bodyelementedittools.js" };
  const FRED_BODYEDITOR_LOGCONSOLE = { src: "js/bodyeditor/logconsole.js" };
  const FRED_BODYEDITOR_SCENE = { src: "js/bodyeditor/scene.js" };
  const FRED_BODYEDITOR_CAMERAMENU = { src: "js/bodyeditor/cameramenu.js" };
  const FRED_BODYEDITOR_CAMERA = { src: "js/bodyeditor/camera.js" };
  const FRED_BODYEDITOR_ANIMATOR = { src: "js/bodyeditor/bodyanimator.js" };
  const FRED_CREATURELIST_DIALOG ={ src: "js/creaturelist/creaturelistdialog.js" };
  const FRED_CREATURELIST_CONTROLLER = { src: "js/creaturelist/controller.js" };
  const FRED_CREATUREDETAILS_DIALOG = { src: "js/creaturedetails/creaturedetailsdialog.js" };
  const FRED_CREATUREDETAILS_CONTROLLER = { src: "js/creaturedetails/controller.js" };
  const FRED_MAINMENU_ABOUTDIALOG = { src: "js/mainmenu/aboutdialog.js" };
  const FRED_MAINMENU_DOWNLOADDIALOG = { src: "js/mainmenu/downloaddialog.js" };
  const FRED_MAINMENU_APPMENU = { src: "js/mainmenu/appmenu.js" };
  const FRED_MAINMENU_CONTROLLER = { src: "js/mainmenu/controller.js" };
  const FRED_NEUROVIEWER_BASEDRAWABLENEURON = { src: "js/neuroviewer/neurons/basedrawableneuron.js" };
  const FRED_NEUROVIEWER_GENERICDRAWABLENEURON = { src: "js/neuroviewer/neurons/genericdrawableneuron.js" };
  const FRED_NEUROVIEWER_RECEPTORDRAWABLENEURON = { src: "js/neuroviewer/neurons/receptordrawableneuron.js" };
  const FRED_NEUROVIEWER_NEUROFACTORY = { src: "js/neuroviewer/neurofactory.js" };

  const FRED_CREATURELIST_VIEW = { src: "js/creaturelist/view.js", divId: "creaturelist-dialog-container" };
  const FRED_CREATUREDETAILS_VIEW = { src: "js/creaturedetails/view.js", divId: "creaturedetails-dialog-container" };
  const FRED_MAINMENU_VIEW = { src: "js/mainmenu/view.js", divId: "menu" };
  const FRED_BODYEDITOR_VIEW = { src: "js/bodyeditor/view.js", divId: "body-editor-container"};
  const FRED_GENOEDITOR_VIEW = { src: "js/genoeditor/view.js", divId: "geno-editor-container" };
  const FRED_NEUROVIEWER_VIEW = { src: "js/neuroviewer/view.js", divId: "neuro-viewer-container" };

  var isHeadScriptLoadingCompleted = false;
  var isDocumentReady = false;

  /*function readDataFromHTML( data ) {

    var genotype = "";

    var start = data.indexOf( '<!--FRED_GEN' );
    if( start > -1 ) {
      start += '<!--FRED_GEN'.length;
      var end = data.indexOf( '-->', start );
      if( end > -1 ) {
        genotype = data.substring( start, end );
      }
    }

    Framsticks.loadCreatureAsNewFile( {
      genotype: genotype,
      name: "",
      info: ""
    } );

  }*/

  function readDataFromJSON( data ) {
    var _genotype = data.genotype || "";

    Framsticks.loadCreatureAsNewFile( {
      genotype: _genotype.replace( '\r\n', '\n' ),
      name: data.name || "",
      info: data.description || ""
    } );
  }

  function generateEditorPageStructure() {

    var body = document.getElementsByTagName( 'body' )[0];
    var mainCenter = document.getElementById( 'main-center' );

    var creatureListDialogContainer = document.createElement( 'div' );
    creatureListDialogContainer.id = "creaturelist-dialog-container";
    body.insertBefore( creatureListDialogContainer, mainCenter  );

    var creatureDetailsDialogContainer = document.createElement( 'div' );
    creatureDetailsDialogContainer.id = "creaturedetails-dialog-container";
    body.insertBefore( creatureDetailsDialogContainer, mainCenter );

    var aboutDialogContainer = document.createElement( 'div' );
    aboutDialogContainer.id = "about-dialog-container";

    var aboutContent = document.createElement( 'div' );
    aboutContent.id = "about-content";
    aboutDialogContainer.appendChild( aboutContent );

    body.insertBefore( aboutDialogContainer, mainCenter );

    var downloadDialogContainer = document.createElement( 'div' );
    downloadDialogContainer.id = "download-dialog-container";
    body.insertBefore( downloadDialogContainer, mainCenter );

    var menu = document.createElement( 'div' );
    menu.id = "menu";
    menu.className = "ui-layout-north";
    body.insertBefore( menu, mainCenter );

    var mainCenter = document.getElementById( "main-center" );
    mainCenter.className = "ui-layout-center";

    var bodyEditorContainer = document.getElementById( "body-editor-container" );

    var logConsoleContainer = document.createElement( 'div' );
    logConsoleContainer.id = "logconsole-container";
    logConsoleContainer.className = "body-editor-south";

    mainCenter.insertBefore( logConsoleContainer, bodyEditorContainer );

    var bodyEditorControlsContainer = document.createElement( 'div' );
    bodyEditorControlsContainer.id = "body-editor-controls-container";
    bodyEditorContainer.appendChild( bodyEditorControlsContainer );

    var mainEast = document.createElement( 'div' );
    mainEast.id = "main-east";
    mainEast.className = "ui-layout-east";
    body.appendChild( mainEast );

    var genoEditorContainer = document.createElement( 'div' );
    genoEditorContainer.id = "geno-editor-container";
    genoEditorContainer.className ="main-east-center";
    mainEast.appendChild( genoEditorContainer );

    var genoEditor = document.createElement( 'div' );
    genoEditorContainer.id = "geno-editor";
    genoEditorContainer.appendChild( genoEditor );

	$("#neuro-viewer-container").remove();
    var neuroViewerContainer = document.createElement( 'div' );
    neuroViewerContainer.id = "neuro-viewer-container";
    neuroViewerContainer.className ="main-east-south";
    mainEast.appendChild( neuroViewerContainer );

}

  function prepareDocument() {

    var scripts;
    var onLoadCompleted;

    if( AppParams[ 'viewer-mode' ] ) {

      scripts = [
        FRED_BODYEDITOR_VIEW,
        FRED_NEUROVIEWER_VIEW
      ];

      onLoadCompleted = onViewerModeBodyScriptsLoadCompleted;

    } else {

      generateEditorPageStructure();

      scripts = [
        FRED_CREATURELIST_VIEW,
        FRED_CREATUREDETAILS_VIEW,
        FRED_MAINMENU_VIEW,
        FRED_BODYEDITOR_VIEW,
        FRED_GENOEDITOR_VIEW,
        FRED_NEUROVIEWER_VIEW
      ];

      onLoadCompleted = onEditorModeBodyScriptsLoadCompleted;

    }

    loadScripts( scripts, onLoadCompleted );

  }

  function onViewerModeBodyScriptsLoadCompleted(){
	$(window).on('resize',function(){
		var containter = $('#neuro-viewer-container');
		NeuroViewerView.resize(containter.width(),containter.height());
	})
	var containter = $('#neuro-viewer-container');
	NeuroViewerView.resize(containter.width(),containter.height());
	loadData()
  }

  function onEditorModeBodyScriptsLoadCompleted() {

    $( 'body' ).layout( {
      applyDemoStyles: false,
      north__resizable: false,
      north__closable: true,
      north__slidable: false,
      north__initHidden: AppParams[ 'viewer-mode' ],
      north__showOverflowOnHover: true,
      east__showOverflowOnHover: true,
      east__size: window.innerWidth * FredConfig.genoEditor.initialWidthRatio,
      east__initHidden: AppParams[ 'viewer-mode' ],
      onresize_end: function ( e ) {
        if ( e === 'center' ) {
          var container = $( "#body-editor-container" );
          BodyEditorView.resizeInEditorMode( container.width(), container.height(), container.offset().top );
        } else if ( e === 'east' ) {
          GenoEditorView.resize();
        }
      }
    } );

    $( '#main-center' ).layout( {
      center__paneSelector: ".body-editor-center",
      center__initHidden: AppParams[ 'viewer-mode' ],
      south__paneSelector: ".body-editor-south",
      south__size: 150,
      south__initHidden: AppParams[ 'viewer-mode' ],
      spacing_open: 8,  // ALL panes
      spacing_closed: 12, // ALL panes
      onresize_end: function ( e ) {
        if ( e === 'center' || e === 'south' ) {
          var container = $( "#body-editor-container" );
          BodyEditorView.resizeInEditorMode( container.width(), container.height(), container.offset().top );
        }
      }
    } );

    $( '#main-east' ).layout( {
      center__paneSelector: ".main-east-center",
      center__initHidden: AppParams[ 'viewer-mode' ],
      south__paneSelector: ".main-east-south",
      south__size: 150,
      south__initHidden: !AppParams["show-nn"],
      spacing_open: 8,  // ALL panes
      spacing_closed: 12, // ALL panes
      onresize_end: function ( e ) {
        if ( e === 'center' || e === 'south' ) {
          GenoEditorView.resize();

          var neuroViewerContainer = $( "#neuro-viewer-container" );
          NeuroViewerView.resize( neuroViewerContainer.width(), neuroViewerContainer.height() );
        }
      }
    } );

    $( "#main-menu" ).find( ".appmenu-item" ).mouseover( function () {
      $( 'body' ).layout().allowOverflow( this );
    } ).mouseout( function () {
      $( 'body' ).layout().resetOverflow( this );
    } );

    var bodyEditorContainer = $( "#body-editor-container" );
    BodyEditorView.resizeInEditorMode( bodyEditorContainer.width(), bodyEditorContainer.height(), bodyEditorContainer.offset().top );

    var neuroViewerContainer = $( "#neuro-viewer-container" );
    NeuroViewerView.resize( neuroViewerContainer.width(), neuroViewerContainer.height() );

    loadData();
  }

  function loadData() {

      if( AppParams["db-url"] ) {

        $.ajax( { type: 'GET',
                  url: AppParams["db-url"],
                  dataType: 'json', 
				  success: readDataFromJSON
				} ) //dataType: 'html'
        //.fail(function() {

          //add error handler

        //});
      }else if(AppParams["genotype"]){
		const startupFile = {
			name: "New creature",
			genotype: AppParams["genotype"],
			info: ""
		};
		Framsticks.loadCreatureAsNewFile(startupFile);
	  }
    else if(AppParams["viewer-mode"] && AppParams["anim-genotypes"]){
      BodyAnimator();
    }
      else {

        Framsticks.loadCreatureAsNewFile( FredConfig.startupFile );

      }

   }

  function onDocumentReady() {
    isDocumentReady = true;
    if( isHeadScriptLoadingCompleted && isDocumentReady ) {
      prepareDocument();
    }
  }

   function loadScripts( scriptSources, onScriptsLoaded, index ) {

      if( typeof index !== 'number' ) {
        index = 0;
      }

      if( index < scriptSources.length ) {

        var script = document.createElement('script');
		const sourceType = scriptSources[index].type?scriptSources[index].type:"text/javascript";
        script.setAttribute("type",sourceType);
        script.setAttribute("src", scriptSources[index].src);
        script.onload = function() {
          loadScripts(scriptSources, onScriptsLoaded, index + 1);
        }

        if( typeof scriptSources[index].parentId === "string" ) {
          document.getElementById( scriptSources[index].parentId ).appendChild(script);
        } else {
          document.getElementsByTagName("head")[0].appendChild(script);
        }

      } else {

        if( typeof onScriptsLoaded === "function" ) {
          onScriptsLoaded();
        }

      }

   }

   function onHeadScriptsLoadingComplete() {
    isHeadScriptLoadingCompleted = true;
    if( isHeadScriptLoadingCompleted && isDocumentReady ) {
      prepareDocument();
    }
   }

  function init() {

    var scripts;

    if( AppParams[ 'viewer-mode' ] ) {

      scripts = [
        LIB_THREE_JS,
        LIB_ORBIT_CONTROLS,
        FRED_UTILS_EVENTS,
        FRED_UTILS_STRINGHELPER,
        FRED_FRAMSTICKS_LOGPARSER,
        FRED_FRAMSTICKS,
        FRED_BODYEDITOR_GRID,
        FRED_BODYEDITOR_TRANSFORMATIONS,
        FRED_BODYEDITOR_RAYCASTER,
        FRED_BODYEDITOR_CONTROLLER,
        FRED_BODYEDITOR_JOINTMESHFACTORY,
        FRED_BODYEDITOR_PARTMESHFACTORY,
        FRED_BODYEDITOR_SCENE,
        FRED_BODYEDITOR_CAMERA,
        FRED_BODYEDITOR_ANIMATOR,
        FRED_NEUROVIEWER_BASEDRAWABLENEURON,
        FRED_NEUROVIEWER_GENERICDRAWABLENEURON,
        FRED_NEUROVIEWER_RECEPTORDRAWABLENEURON,
        FRED_NEUROVIEWER_NEUROFACTORY,
      ];

    } else {

      scripts = [
        LIB_JQUERY_UI,
        LIB_JQUERY_LAYOUT,
        LIB_THREE_JS,
        LIB_ORBIT_CONTROLS,
        LIB_TRANSFORM_CONTROLS,
        LIB_FILE_SAVER,
        FRED_UTILS_FILEHANDLER,
        FRED_UTILS_EVENTS,
        FRED_UTILS_STRINGHELPER,
        FRED_FRAMSTICKS_LOGPARSER,
        FRED_FRAMSTICKS,
        LIB_ACE,
        FRED_BODYEDITOR_GRID,
        FRED_BODYEDITOR_SCENEMOUSEINPUT,
        FRED_BODYEDITOR_TRANSFORMATIONS,
        FRED_BODYEDITOR_RAYCASTER,
        FRED_BODYEDITOR_CONTROLLER,
        FRED_BODYEDITOR_PROPERTIESEDITPANEL,
        FRED_BODYEDITOR_ACTIONMENU,
        FRED_BODYEDITOR_JOINTMESHFACTORY,
        FRED_BODYEDITOR_PARTMESHFACTORY,
        FRED_BODYEDITOR_BODYELEMENTEDITTOOLS,
        FRED_BODYEDITOR_LOGCONSOLE,
        FRED_BODYEDITOR_SCENE,
        FRED_BODYEDITOR_CAMERA,
        FRED_BODYEDITOR_CAMERAMENU,
        FRED_CREATURELIST_DIALOG,
        FRED_CREATURELIST_CONTROLLER,
        FRED_CREATUREDETAILS_DIALOG,
        FRED_CREATUREDETAILS_CONTROLLER,
        FRED_MAINMENU_ABOUTDIALOG,
        FRED_MAINMENU_DOWNLOADDIALOG,
        FRED_MAINMENU_APPMENU,
        FRED_MAINMENU_CONTROLLER,
        FRED_NEUROVIEWER_BASEDRAWABLENEURON,
        FRED_NEUROVIEWER_GENERICDRAWABLENEURON,
        FRED_NEUROVIEWER_RECEPTORDRAWABLENEURON,
        FRED_NEUROVIEWER_NEUROFACTORY,
      ];

    }

    loadScripts( scripts, onHeadScriptsLoadingComplete );
    $(document).ready( onDocumentReady );
  }

  init();

}());