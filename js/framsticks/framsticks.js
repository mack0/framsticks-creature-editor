var Framsticks = (function () {

  var gcm = new Module.DefaultGenoConvManager();
  gcm.addDefaultConverters();
  var dummyGeno = new Module.Geno().useConverters(gcm);
  Module.destroy(dummyGeno);

  var validators = new Module.Validators();
  var dummyGeno = new Module.Geno().useValidators(validators);
  Module.destroy(dummyGeno);
  var modelValidator = new Module.ModelGenoValidator();
  validators.append(modelValidator);

  var framsFileSystem = new Module.StdioFileSystem_autoselect();
  var saveFileHelper = new Module.SaveFileHelper();
  var neuroLayoutFunctionHelper = new Module.NNLayoutFunctionHelper();

  var currentCreature;
  var creatures = [];
  var creatureIndex = 0;

  var model;
  var defaultShapeType = Module.Model["SHAPETYPE_UNKNOWN"];
  var neuroLayoutModel;
  var validationResult;

  var logger = new FramsticksSDKLogParser( );
  logger.setModel( model );

  function readLogAndGetGenotypeMessages() {
    var result = logger.readLogMessages();

    if( result ) {
	  validationResult = result.genoLineMsgs;
      AppEvents.publish( "modelLogReaded", result.allMessages );
      return result.genoLineMsgs;
    }
	validationResult = null;
  }

  function getValidationResult(){
	return validationResult;
  }

  function loadModel( genotype ) {
    var stringObj = new Module.SString();
    stringObj.set(genotype);

    var genoObj = new Module.Geno(stringObj);
    Module.destroy(stringObj);

    if (model) {
      Module.destroy(model);
    }
    model = new Module.Model(genoObj, defaultShapeType, true);
	// this is done to ignore the "Model shape type '%s' does not match the declared type '%s'"" warning
	// because the model does not log it when shapetype is unknown
	// and the constructor automatically declares the shapetype to that of the genotype
	model.declareShapeType(Module.Model.SHAPETYPE_UNKNOWN);
    logger.setModel( model );
	readLogAndGetGenotypeMessages();

    updateNeuroLayoutModel();

    model.open();
    Module.destroy(genoObj);
  }

  function updateNeuroLayoutModel() {
    if( neuroLayoutModel ) {
      Module.destroy( neuroLayoutModel );
    }
    neuroLayoutModel = new Module.NNLayoutState_Model_Fred( model );
    neuroLayoutFunctionHelper.doLayout( FredConfig.neuroViewer.layoutType , neuroLayoutModel );
  }

  function publishBrainDataChange ( eventName ) {
    AppEvents.publish( eventName, {
      getElements: function () {
        return neuroLayoutModel.GetElements();
      },
      getValueXYWH: function ( i ) {
        return neuroLayoutModel.GetValueXYWH( i );
      },
      getNeuro: function ( i ) {
        return model.getNeuro( i );
      }
    } );
  }

  function publishGenotypeChanged( isNew ) {

    AppEvents.publish("genotypeChanged_bodyData", {
      getPartCount: function () {
        return model.getPartCount()
      },
      getJointCount: function () {
        return model.getJointCount()
      },
      getPart: function (i) {
        return model.getPart(i)
      },
      getJoint: function (i) {
        return model.getJoint(i)
      },
      isNewGenotype: isNew
    });

    publishBrainDataChange( "genotypeChanged_brainData" );

  }

  function loadCreatureAsNewFile( creatureData ) {

    if (typeof creatureData.genotype === "string") {
      loadModel( creatureData.genotype );

      currentCreature = {
        id: 1,
        name: creatureData.name,
        info: creatureData.info,
        genotype: creatureData.genotype
      };
      creatures = [ currentCreature ];

      publishGenotypeChanged( true );
      publishModelUpdated( true );
    }

  }

  function loadModelFromGenotype( genotype, isNew ) {
    if (typeof genotype === "string") {
      loadModel( genotype );

      publishGenotypeChanged( isNew );
    }
  };

  function loadCreaturesFromFileData(fileData) {
    FS.writeFile('/current.gen', fileData);
    var loadedGenotypes = [];

    var loader = new Module.GenotypeMiniLoader('/current.gen');
    var geno = loader.loadNextGenotype();

    var nextID = 1;
    while(geno && geno.ptr != 0) {
      loadedGenotypes.push({
        id: nextID++,
        name: geno.get_name().c_str(),
        info: geno.get_info().c_str(),
        genotype: geno.get_genotype().c_str(),
      });
      geno = loader.loadNextGenotype();
    }

    Module.destroy(loader);

    if( loadedGenotypes.length > 0 ) {
      creatures = loadedGenotypes;

      if( loadedGenotypes.length > 1 ) {
        currentCreature = null;
        AppEvents.publish( "openCreatureListDialog" );
        return true;
      }

      currentCreature = loadedGenotypes[0];
      loadModelFromGenotype( loadedGenotypes[0].genotype, true );
      publishModelUpdated( true );
      return true;
    }
    return false;
  };

  function getModel(){
	return model;
  }

  function updateModel( preventEventPublishing ) {

    model.close();
    model.open();

    logger.saveSDKLog();
	readLogAndGetGenotypeMessages();

    if( !preventEventPublishing ) {
		publishModelUpdated( false );
    }

  };

  function publishModelUpdated( isNew ) {

    AppEvents.publish("modelUpdated", {
      genotype: getModelGenotype(),
      isNewGenotype: isNew
    });

  };

  function getModelGenotype() {

    var geno = new Module.Geno();
    model.makeGeno(geno,false,true,true);
    var result = geno.getGenesAndFormat().c_str();
    Module.destroy( geno );
    return result;

  }

  function setModelShapeType(sh){
	if(typeof sh !== "long" && Object.hasOwn(sh,"value")){
		sh = sh.value
	}
	model.declareShapeType(sh);
	defaultShapeType = sh;
	updateModel();
  }

  function getModelShapeTypes() {
    var shapes = [ ];
    shapes["SHAPETYPE_UNKNOWN"] = { name: "Unknown", value: Module.Model["SHAPETYPE_UNKNOWN"] };
    shapes["SHAPETYPE_ILLEGAL"] = { name: "Illegal", value: Module.Model["SHAPETYPE_ILLEGAL"] };
    shapes["SHAPETYPE_BALL_AND_STICK"] = { name: "Ball and stick", value: Module.Model["SHAPETYPE_BALL_AND_STICK"] };
    shapes["SHAPETYPE_SOLIDS"] = { name: "Solids", value: Module.Model["SHAPETYPE_SOLIDS"] };
    return shapes;
  }

  function getPartShapes() {
    var shapes = [ ];
    shapes["SHAPE_BALL"] = { name: "Ball", value: Module.Part["SHAPE_BALL"] };
    shapes["SHAPE_ELLIPSOID"] = { name: "Elipsoid", value: Module.Part["SHAPE_ELLIPSOID"] };
    shapes["SHAPE_CUBOID"] = { name: "Cuboid", value: Module.Part["SHAPE_CUBOID"] };
    shapes["SHAPE_CYLINDER"] = { name: "Cylinder", value: Module.Part["SHAPE_CYLINDER"] };
    return shapes;
  }

  function getJointShapes() {
    var shapes = [ ];
    shapes["SHAPE_STICK"] = { name: "Stick", value: Module.Joint["SHAPE_STICK"] };
    shapes["SHAPE_FIXED"] = { name: "Fixed", value: Module.Joint["SHAPE_FIXED"] };
	shapes["SHAPE_HINGE_X"] = { name: "X Hinge", value: Module.Joint["SHAPE_HINGE_X"] };
    shapes["SHAPE_HINGE_XY"] = { name: "XY Hinge", value: Module.Joint["SHAPE_HINGE_XY"] };
    return shapes;
  }

  function getNeuroClassHints() {
    var hints = [ ];
    hints["Invisible"] = { name: "Invisible", value: Module.NeuroClass["Invisible"] };
    hints["DontShowClass"] = { name: "DontShowClass", value: Module.NeuroClass["DontShowClass"] };
    hints["AtFirstPart"] = { name: "AtFirstPart", value: Module.NeuroClass["AtFirstPart"] };
    hints["AtSecondPart"] = { name: "AtSecondPart", value: Module.NeuroClass["AtSecondPart"] };
    hints["EffectorClass"] = { name: "EffectorClass", value: Module.NeuroClass["EffectorClass"] };
    hints["ReceptorClass"] = { name: "ReceptorClass", value: Module.NeuroClass["ReceptorClass"] };
    hints["IsV1BendMuscle"] = { name: "V1BendMuscle", value: Module.NeuroClass["IsV1BendMuscle"] };
    hints["IsV1RotMuscle"] = { name: "V1RotMuscle", value: Module.NeuroClass["IsV1RotMuscle"] };
    hints["IsLinearMuscle"] = { name: "LinearMuscle", value: Module.NeuroClass["IsLinearMuscle"] };
    return hints;
  }

  function partsNotJoined( part1, part2 ) {
    return ( model.findJoint( part1, part2 ) < 0 ) && ( model.findJoint( part2, part1 ) < 0 );
  }

  function addNewPart( posX, posY, posZ )  {
    var part;

    part = model.addNewPart( Module.Part["SHAPE_BALL"] );
    if( part ) {
      part.get_p().set_x( posX );
      part.get_p().set_y( posY );
      part.get_p().set_z( posZ );
    }

    updateModel();
    return part;
  }

  function addNewJoint( part1, part2 ) {
    var joint = model.addNewJoint( part1, part2, Module.Joint["SHAPE_STICK"] );
    updateModel();
    return joint;
  }

  function removePart( part ) {
    if( part ) {
      model.removePart( part.get_refno() );
      updateModel();
      publishBrainDataChange( "bodyElementRemoved_brainData" );
    }
  }

  function removeJoint( joint ) {
    if( joint ) {
      model.removeJoint( joint.get_refno() );
      updateModel();
      publishBrainDataChange( "bodyElementRemoved_brainData" );
    }
  }

  function getPartCount( ) {
    if( model ) {
      return model.getPartCount();
    }
  }

  function saveCurrentGenotype() {
    if( currentCreature ) {
      currentCreature.genotype = getModelGenotype();
    }
  }

  function generateFileData() {

    saveCurrentGenotype();

    var f = saveFileHelper.Vfopen("/current.gen", "w");
    var miniGeno = new Module.GenotypeMini();
    var param = new Module.Param(saveFileHelper.getMinigenotype_paramtab(), miniGeno);
    miniGeno.clear();

    for (var i = 0; i < creatures.length; i++)
    {
      miniGeno.get_name().set( creatures[i].name );
      miniGeno.get_info().set( creatures[i].info );
      miniGeno.get_genotype().set( creatures[i].genotype );
      param.saveMultiLine(f, "org");
    }

    Module.destroy(f);
    Module.destroy(param);
    Module.destroy(miniGeno);

    return FS.readFile('/current.gen', { encoding: 'utf8' } );

  }

  function getAllCreatures() {
    return { creatures: creatures,
             currentCreature: currentCreature };
  }

  function addNewGenotype(){
    return addSpecificGenotype(FredConfig.startupFile.genotype);
  }

  function addSpecificGenotype(genotypeString) {
    var id = 0;
    for(var i=0; i<creatures.length; ++i) {
      id = Math.max(creatures[i].id, id);
    }
    id++;

    var newGenotype = {
      id: id,
      name: FredConfig.startupFile.name + " " + String(id),
      genotype: genotypeString,
      info: FredConfig.startupFile.info
    };

    creatures.push( newGenotype );
    return newGenotype;
  }

  function selectCreature( id ) {
    saveCurrentGenotype();

    for(var i=0; i<creatures.length; ++i) {
      if( creatures[i].id == id ) {
        selectCurrentCreature( creatures[i] );
      }
    }
  }

  function nextCreature(isNew){
    creatureIndex++;
    creatureIndex %= creatures.length;

    selectSpecificCreature(creatures[creatureIndex],isNew);
  }

  function selectCurrentCreature( creature ) {
    return selectSpecificCreature(creature,true);
  }

  function selectSpecificCreature( creature, isNew ) {
    currentCreature = creature;
    loadModelFromGenotype( creature.genotype, isNew );
    publishModelUpdated( isNew );
  }

  function deleteCreature( id ) {

    var newCreaturesList = [];

    for(var i=0; i<creatures.length; ++i) {

      if( creatures[i].id !== id ) {
        newCreaturesList.push( creatures[i] );
      } else if ( currentCreature && currentCreature.id == creatures[i].id ) {
        currentCreature = null;
      }

    }

    creatures = newCreaturesList;

  }

  function updateGenotype( genotype ) {
    loadModelFromGenotype( genotype, false );
  }

  function getCurrentCreature() {
    return currentCreature;
  }

  function getCreatureIndex(){
    return creatureIndex;
  }

  return {
    updateGenotype: updateGenotype,
    loadCreaturesFromFileData: loadCreaturesFromFileData,
    loadCreatureAsNewFile: loadCreatureAsNewFile,
    updateModel: updateModel,
    publishModelUpdated: publishModelUpdated,
    publishGenotypeChanged: publishGenotypeChanged,
    getPartShapes: getPartShapes,
    getJointShapes: getJointShapes,
    partsNotJoined: partsNotJoined,
    addNewPart: addNewPart,
    addNewJoint: addNewJoint,
    removePart: removePart,
    removeJoint: removeJoint,
    readLogAndGetGenotypeMessages: readLogAndGetGenotypeMessages,
    generateFileData: generateFileData,
    getAllCreatures: getAllCreatures,
    addNewGenotype: addNewGenotype,
    selectCreature: selectCreature,
    deleteCreature: deleteCreature,
    getCurrentCreature: getCurrentCreature,
    getNeuroClassHints: getNeuroClassHints,
    getPartCount: getPartCount,
    getModel: getModel,
    getModelShapeTypes: getModelShapeTypes,
    setModelShapeType: setModelShapeType,
    addSpecificGenotype: addSpecificGenotype,
    nextCreature: nextCreature,
    getCreatureIndex: getCreatureIndex,
	getValidationResult: getValidationResult,
  };
})();
