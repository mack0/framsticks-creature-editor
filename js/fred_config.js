var FredConfig = (function() {

  var config = {

    bodyEditor : {

      canvas: {
        id: "body-editor-canvas"
      },

      grid: {
        size: 50,
        interval: 1,
        color: 0x202020,
        centerLineColor: 0x282828
      },

      axis: {
        size: 50
      },

      camera: {
        fieldOfView: 75,
        near: 0.1,
        far: 1000,
        autoRotateSpeed: 2,
        defaultSettings: {
          target: {
            x: 0,
            y: 0,
            z: 0
          },
          position: {
            x: 0.36,
            y: 0.36,
            z: 0.36
          },
        }
      },

      light: { //locations and intensities adjusted experimentally to maximize the variation in the brightness of surfaces (more lights would be better)
        ambient: {
          color: 0xdddddd
        },
        directional: {
          top: {
            color: 0xffffff,
            intensity: 3.0,
            positionX: -31,
            positionY: 15,
            positionZ: 61
          },
          bottom: {
            color: 0xffffff,
            intensity: 1.5,
            positionX: -23,
            positionY: -49,
            positionZ: -51
          }
        }
      },

      raycaster: {
        focusedElement: {
          color: 0x900090
        }
      },

      selectedElement: {
        wireframeColor: 0xFFFF00
      },

      geometry: {
		material:{
			roughness: 0.4,
			metalness: 0
		},
        part: {
          defaultShape: {
            radius: Module.Part.prototype.BALL_AND_STICK_RADIUS,
            segments: 24
          },
          ellipsoidShape: {
            radius: 1,
            segments: 32
          }
        },
        joint: {
          cylinderShape: {
			thickness: 0.8, // multiplied by part radius
            radiusSegments: 24,
            isTransparent: false,
            opacity: 1
          },
          linkShape: {
            radius: 0.04,
            radiusSegments: 10,
            isTransparent: true,
            opacity: 0.4
          }
        }
      },

      edit: {
        newPartDistanceFromCamera: 8.5
      },

      animator: {
        genotypeSplit: '\0'
      }

    },

    genoEditor : {
      initialWidthRatio: 0.3
    },

    fileHandler: {
      maxSizeInMB: 1,
      defaultFileName: "framsticks.gen"
    },

    startupFile: {
      name: "New creature",
      genotype: "//0\np:",
      info: ""
    },

    neuroViewer: {
      layoutType: 2,
      canvas: {
        id: "neuro-viewer-canvas",
        drawMarginInPx: 20,
		drawMarginInPxInViewerMode: 0,
		heightPercentViewerMode: 25,
        font: "14px Lucida Grande, sans-serif",
        strokeStyle: '#DEDEDE'
      },
    },

    SDK: {
      validation: {
        minimalLogLevel: -1
      }
    }

  };

  config.bodyEditor.geometry.joint.cylinderShape.radius = config.bodyEditor.geometry.part.defaultShape.radius * config.bodyEditor.geometry.joint.cylinderShape.thickness;

  return config;

}());
