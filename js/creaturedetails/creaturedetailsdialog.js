var CreatureDetailsDialog = function ( containerId, width, height ) {

  var dialog;

  var inputFields = [];

  function init() {

    var container = document.getElementById( containerId );
    if( container ) {

      var form = document.createElement( 'form' );
      var fieldset = document.createElement( 'fieldset' );
      form.appendChild( fieldset );

      var nameFieldDiv = document.createElement( 'div' );
      nameFieldDiv.classList.add( "dialog-field" );

      var nameLabel = document.createElement( 'label' );
      nameLabel.htmlFor = "name";
      nameLabel.textContent = AppResources.get( "name_label" );
      nameFieldDiv.appendChild( nameLabel );

      var nameInput = document.createElement( 'input' );
      nameInput.id = "creaturedetails-name-input";
      nameInput.type = "text";
      nameInput.name = "name";
      nameInput.onblur = handleInputBlur;
      nameInput.classList.add( "text" );
      nameInput.classList.add( "ui-widget-content" );
      nameInput.classList.add( "ui-corner-all" );
      nameInput.classList.add( "dialog-textinput" );

      inputFields[ nameInput.id ] = nameInput;

      nameFieldDiv.appendChild( nameInput );

      fieldset.appendChild( nameFieldDiv );

      var infoFieldDiv = document.createElement( 'div' );
      infoFieldDiv.classList.add( "dialog-field" );

      var infoLabel = document.createElement( 'label' );
      infoLabel.htmlFor = "info";
      infoLabel.textContent = AppResources.get( "info_label" );
      infoFieldDiv.appendChild( infoLabel );

      var infoArea = document.createElement( 'textArea' );
      infoArea.id = "creaturedetails-info-input";
      infoArea.name = "info";
      infoArea.rows = 10;
      infoArea.onblur = handleInputBlur;
      infoArea.classList.add( "text" );
      infoArea.classList.add( "ui-widget-content" );
      infoArea.classList.add( "ui-corner-all" );
      infoArea.classList.add( "dialog-textarea" );
      infoFieldDiv.appendChild( infoArea );

      inputFields[ infoArea.id ] = infoArea;

      fieldset.appendChild( infoFieldDiv );

      container.appendChild( form );

      dialog = $( '#'+containerId ).dialog({
        title: AppResources.get( "creatureDetailsDialog_title" ),
        autoOpen: false,
        height: height,
        width: width,
        modal: true,
        buttons: {
          Close: function() {
            dialog.dialog("close");
          }
        }
      });
    }

  }

  function reloadInputValue( key ) {
    if( inputFields.hasOwnProperty( key ) && typeof inputFields[ key ].onvalueload === "function" ) {
      inputFields[ key ].onvalueload( inputFields[ key ] );
    }
  }

  function handleInputBlur(event) {
    if( event && event.target ) {
      var input = inputFields[ event.target.id ];
      if ( typeof input.onvaluechange === "function" ) {
        input.onvaluechange( input.value );
      }
    }
  }

  function open( ) {

    for( var key in inputFields ) {
      reloadInputValue( key )
    }

    dialog.dialog( "open" );

  }

  function getInput( fieldName ) {
    return inputFields[ "creaturedetails-" + String(fieldName) + "-input" ];
  }

  init();

  return {
    open: open,
    getInput: getInput
  };

};
