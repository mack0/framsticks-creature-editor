var creatureDetailsController = function ( creatureDetailsDialog ) {

  function openDialog( ) {
    creatureDetailsDialog.open( );
  }

  creatureDetailsDialog.getInput( 'name' ).onvalueload = function ( input ) {
    var creature = Framsticks.getCurrentCreature();
    if( creature ) {
      input.value = creature.name;
    }
  }

  creatureDetailsDialog.getInput( 'name' ).onvaluechange = function ( value ) {
    var creature = Framsticks.getCurrentCreature();
    if( creature ) {
      creature.name = value;
    }
  }

  creatureDetailsDialog.getInput( 'info' ).onvalueload = function ( input ) {
    var creature = Framsticks.getCurrentCreature();
    if( creature ) {
      input.value = creature.info;
    }
  }

  creatureDetailsDialog.getInput( 'info' ).onvaluechange = function ( value ) {
    var creature = Framsticks.getCurrentCreature();
    if( creature ) {
      creature.info = value;
    }
  }

  AppEvents.register( 'openCreatureDetailsDialog', openDialog );

};
