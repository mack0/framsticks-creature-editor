define("ace/mode/matching_brace_outdent",["require","exports","module","ace/range"], function(require, exports, module) {
"use strict";

var Range = require("../range").Range;

var MatchingBraceOutdent = function() {};

(function() {

    this.checkOutdent = function(line, input) {
        if (! /^\s+$/.test(line))
            return false;

        return /^\s*\}/.test(input);
    };

    this.autoOutdent = function(doc, row) {
        var line = doc.getLine(row);
        var match = line.match(/^(\s*\})/);

        if (!match) return 0;

        var column = match[1].length;
        var openBracePos = doc.findMatchingBracket({row: row, column: column});

        if (!openBracePos || openBracePos.row == row) return 0;

        var indent = this.$getIndent(doc.getLine(openBracePos.row));
        doc.replace(new Range(row, 0, row, column-1), indent);
    };

    this.$getIndent = function(line) {
        return line.match(/^\s*/)[0];
    };

}).call(MatchingBraceOutdent.prototype);

exports.MatchingBraceOutdent = MatchingBraceOutdent;
});

define("ace/mode/f0_highlight_rules",["require","exports","module","ace/lib/oop","ace/mode/text_highlight_rules"], function(require, exports, module) {
"use strict";

var oop = require("../lib/oop");
var TextHighlightRules = require("./text_highlight_rules").TextHighlightRules;

var MyNewHighlightRules = function() {
   this.$rules = {
     "start" : [
			{
				token: "genotype", // line comment starting with //
				regex: "^\\s*//.*$"
			},
			{
				token: "genotype", // block comment starting with /* */
				regex: "^\\s*/\\*.*\\*/"
			},
            {

                token : "storage",
                regex : "^\\s*[pjnc]:"
            },
            {
                token : "string", // single line
                regex : '["](?:(?:\\\\.)|(?:[^"\\\\]))*?["]'
            }, {
                token : "string", // single line
                regex : "['](?:(?:\\\\.)|(?:[^'\\\\]))*?[']"
            },
            {
                token : "keyword.operator",
                regex : "="
            },
            {
                token : "variable",
                regex : 'rotstif|stif|stam|ing|as|vs|fr|dn|sx|sy|sz|p1|p2|rx|ry|rz|dx|dy|dz|sh|vr|vg|vb|x|y|z|m|s|i|p|j|d|h'
            },
            {
                token : "constant.numeric", // float
                regex : /[+-]?\d[\d_]*(?:(?:\.[\d_]*)?(?:[eE][+-]?[\d_]+)?)?\b/
            },
        ]
      }
  }

oop.inherits(MyNewHighlightRules, TextHighlightRules);

exports.MyNewHighlightRules = MyNewHighlightRules;

});

define("ace/mode/f0",["require","exports","module","ace/lib/oop","ace/mode/text","ace/tokenizer","ace/mode/matching_brace_outdent","ace/mode/f0_highlight_rules","ace/worker/worker_client"], function(require, exports, module) {
"use strict";

var oop = require("../lib/oop");
var TextMode = require("./text").Mode;
var Tokenizer = require("../tokenizer").Tokenizer;
var MatchingBraceOutdent = require("./matching_brace_outdent").MatchingBraceOutdent;
var MyNewHighlightRules = require("./f0_highlight_rules").MyNewHighlightRules;

var Mode = function() {
    this.HighlightRules = MyNewHighlightRules;
    this.$outdent = new MatchingBraceOutdent();
};
oop.inherits(Mode, TextMode);

(function() {
    this.lineCommentStart = "//";
    this.blockComment = {start: "/*", end: "*/"};

    this.getNextLineIndent = function(state, line, tab) {
        var indent = this.$getIndent(line);
        return indent;
    };

    this.checkOutdent = function(state, line, input) {
        return this.$outdent.checkOutdent(line, input);
    };

    this.autoOutdent = function(state, doc, row) {
        this.$outdent.autoOutdent(doc, row);
    };

    var WorkerClient = require("ace/worker/worker_client").WorkerClient;
    this.createWorker = function(session) {
        var worker = new WorkerClient(["ace"], "ace/mode/f0_worker", "F0Worker");
        worker.attachToDocument(session.getDocument());

        worker.on("workerinvoked", function(results) {
          var validator = session.$validator;
          if( validator ) {
            var genoMsgs = validator.getValidationResult();
            if( genoMsgs ) {
              session.setAnnotations( genoMsgs );
            }
          }
        });

        worker.on("terminate", function() {
            session.clearAnnotations();
        });

        return worker;
    }

}).call(Mode.prototype);

exports.Mode = Mode;
});
