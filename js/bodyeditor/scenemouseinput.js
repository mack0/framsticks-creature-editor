var SceneMouseInput = function ( renderer ) {

  const LMB = 0;
  const RMB = 2;

  var mousePosition = new THREE.Vector2(Number.POSITIVE_INFINITY, Number.POSITIVE_INFINITY);
  var mouseDownPosition =  new THREE.Vector2(Number.POSITIVE_INFINITY, Number.POSITIVE_INFINITY);

  var mouseDown = false;
  var offsetTop = 0;
  var offsetLeft = 0;

  var handlers = {
    onLeftButtonClick: null,
    onRightButtonClick: null,
    onPositionChanged: null,
    onButtonDown: null
  };

  function init() {
    if( renderer && renderer.getContext() && renderer.getContext().canvas ) {
      renderer.getContext().canvas.addEventListener('mousemove', handleCanvasMouseMove, false);
      renderer.getContext().canvas.addEventListener('mousedown', handleCanvasMouseDown, false);
      renderer.getContext().canvas.addEventListener('mouseup', handleCanvasMouseUp, false);
      renderer.getContext().canvas.addEventListener('mouseleave', handleCanvasLeave, false);
      document.addEventListener('contextmenu', handleDocumentContextWindow, false);
    }
  }

  function getMouseCoords(clientX, clientY, viewSize) {
    // calculate mouse position in normalized device coordinates
    // (-1 to +1) for both components
    var result = new THREE.Vector2();
    result.x = ( (clientX-offsetLeft) / viewSize.width ) * 2 - 1;
    result.y = - ( (clientY-offsetTop) / viewSize.height ) * 2 + 1;
    return result;
  }

  function handleDocumentContextWindow(event) {
    event.preventDefault();
  }

  function handleCanvasMouseMove( event ) {
	var target = new THREE.Vector2( );
    mousePosition = getMouseCoords(event.clientX, event.clientY, renderer.getSize(target));
    if( typeof handlers.onPositionChanged === "function" ) {
      handlers.onPositionChanged( { positionInDeviceCoords: mousePosition, isDown: mouseDown } );
    }
  }

  function handleCanvasMouseDown( event ) {
	var target = new THREE.Vector2( );
    mouseDownPosition = getMouseCoords(event.clientX, event.clientY, renderer.getSize(target	));
    mouseDown = true;
    if( typeof handlers.onButtonDown === "function" ) {
      handlers.onButtonDown( { positionInDeviceCoords: mousePosition, isDown: true } );
    }
  }

  function handleCanvasMouseUp( event ) {

    mouseDown = false;

    if( mouseDownPosition.distanceTo( mousePosition ) === 0 ) {

      if( event.button === LMB && typeof handlers.onLeftButtonClick === "function" ) {
        handlers.onLeftButtonClick( { positionInDeviceCoords: mousePosition, isDown: false, clientX: event.clientX-offsetLeft, clientY: event.clientY-offsetTop } );
      } else if ( event.button === RMB && typeof handlers.onRightButtonClick === "function" ) {
        handlers.onRightButtonClick( { positionInDeviceCoords: mousePosition, isDown: false, clientX: event.clientX-offsetLeft, clientY: event.clientY-offsetTop } );
      }

    }

  }

  function handleCanvasLeave( event ) {
    mouseDown = false;
  }

  init();

  return {
    getEventHandlers: function() {
      return handlers;
    },
    setOffsetTop: function( value ) {
      offsetTop = value;
    }
  }
};
