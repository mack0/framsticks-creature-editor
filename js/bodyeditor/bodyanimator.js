var BodyAnimator = function(){
    const miliSeconds = 1000;

    function delay(time) {
        return new Promise(resolve => setTimeout(resolve, time));
    };

    function isLastGenotype(){
        return Framsticks.getAllCreatures().creatures.length === (Framsticks.getCreatureIndex()+1);
    }

    async function run(){
        while(true){
            const value = isLastGenotype()?AppParams["anim-delay-end"]:AppParams["anim-delay"];
            await delay(value * miliSeconds).then(function(){
                Framsticks.nextCreature(AppParams["anim-reset-zoom"]);
            })
        }
    };

    function loadGenotypes(){
        const genotypes = AppParams["anim-genotypes"].split(FredConfig.bodyEditor.animator.genotypeSplit);
        for(let i=0; i < genotypes.length;i++){
            Framsticks.addSpecificGenotype(genotypes[i]);
        }
        Framsticks.selectCreature(1);
    }

    function init(){
        if(AppParams["viewer-mode"] && AppParams["anim-genotypes"]){
            loadGenotypes();
            run();
        }
    };
    init();
}