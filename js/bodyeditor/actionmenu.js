var ActionMenu = function (menuId, containerId, actions) {

  var mainDiv;
  var menuList;
  var menuListItems = [];
  var dataForAction;

  function handleMenuItemClick(event) {
    if( event && event.target ) {
      var item = menuListItems[ event.target.id ];
      if( item && typeof item.onitemclick === 'function' ) {
        item.onitemclick( dataForAction );
      }
    }
    close();
  }

  function getMenuItemFullId( id ) {
    return String( menuId ) + "-" + String( id );
  }

  function init() {
    mainDiv = document.createElement( "div" );
    mainDiv.id = menuId;
    mainDiv.className = "context-menu";

    menuList = document.createElement( "ul" );
    menuList.className = "context-menu-items";
    mainDiv.appendChild( menuList );

    for(var i=0; i<actions.length; ++i) {
      menuListItem = document.createElement( "li" );
      menuListItem.classList.add( "context-menu-item" );
      menuListItem.classList.add( "non-selectable-text-content" );
      menuListItem.textContent = actions[ i ].text;

      var itemId = getMenuItemFullId( actions[ i ].id );
      menuListItem.id = itemId;
      menuListItems[ itemId ] = menuListItem;
      menuList.appendChild( menuListItem );
    }

    var container = document.getElementById( containerId );
    if( container ) {
      container.appendChild( mainDiv )
      close();
    }

    menuList.addEventListener('click', handleMenuItemClick, false);
  }

  init();

  function open(x, y, data) {
    mainDiv.setAttribute( 'style', 'position: absolute; left:' + String(x) + "px; top: " + String(y) + "px;" );
    dataForAction = data;
  }

  function close() {
    mainDiv.setAttribute( 'style', 'display: none;' );
    dataForAction = null;
  }

  function getMenuItem( id ) {
    return menuListItems[ getMenuItemFullId( id ) ];
  }

  return {
    open: open,
    close: close,
    getMenuItem: getMenuItem,
  };

};
