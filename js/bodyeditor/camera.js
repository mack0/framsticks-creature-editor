function Camera( config, rendererDomElement, autoRotate ) {

  var perspectiveCamera;
  var cameraControl;

  function init() {
    perspectiveCamera = new THREE.PerspectiveCamera(config.fieldOfView, window.innerWidth/window.innerHeight, config.near, config.far);
    perspectiveCamera.up.set(0,0,1);

    cameraControl = new OrbitControls(perspectiveCamera, rendererDomElement);
    cameraControl.autoRotate = autoRotate;
    cameraControl.autoRotateSpeed = config.autoRotateSpeed;
    cameraControl.enableKeys = false;
  }
  init();

  function updateModelBox( modelBox, box ) {
    modelBox.min.x = Math.min( modelBox.min.x, box.min.x );
    modelBox.min.y = Math.min( modelBox.min.y, box.min.y );
    modelBox.min.z = Math.min( modelBox.min.z, box.min.z );

    modelBox.max.x = Math.max( modelBox.max.x, box.max.x );
    modelBox.max.y = Math.max( modelBox.max.y, box.max.y );
    modelBox.max.z = Math.max( modelBox.max.z, box.max.z );
  }

  function getBoundingBox( object ) {
    var box = new THREE.Box3();
    box.setFromObject( object );
    return box;
  }

  function calculateCameraSetting( meshes ) {

    if( meshes.length == 0 ) {
      return config.defaultSettings;
    }

    var modelBox = new THREE.Box3();
    for(var i=0; i<meshes.length; ++i) {
      var mesh = meshes[ i ];
      var box = getBoundingBox( mesh );
      updateModelBox( modelBox, box )
    }

    var modelSphere = new THREE.Sphere();
	modelBox.getBoundingSphere(modelSphere);

    return {
      target: {
        x: modelSphere.center.x,
        y: modelSphere.center.y,
        z: modelSphere.center.z
      },
      position: {
        x: modelSphere.center.x + modelSphere.radius,
        y: modelSphere.center.y + modelSphere.radius,
        z: modelSphere.center.z + modelSphere.radius
      }
    }

  }

  this.zoomAll = function( meshes ) {

    var settings = calculateCameraSetting( meshes );
    cameraControl.target.set( settings.target.x, settings.target.y, settings.target.z );
    perspectiveCamera.position.x = ( settings.position.x );
    perspectiveCamera.position.y = ( settings.position.y );
    perspectiveCamera.position.z = ( settings.position.z );
    cameraControl.update();

  }

  this.getPerspectiveCamera = function() {
    return perspectiveCamera;
  }

  this.getCameraControl = function() {
    return cameraControl;
  }

};