var BodyEditorView = (function(config, controller, partShapes, jointShapes)
{

  var container = document.getElementById("body-editor-container");
  var renderer = new THREE.WebGLRenderer( {
    antialias: true,
    devicePixelRatio: window.devicePixelRatio || 1
  } );
  renderer.setSize( window.innerWidth, window.innerHeight );
  container.appendChild( renderer.domElement );
  renderer.domElement.id = config.canvas.id;

  var scene = new Scene();
  var camera = new Camera( config.camera, renderer.domElement, AppParams["autorotate"] );

  if( !AppParams['viewer-mode'] ) {

    var partPropertiesEditPanel = new PropertiesEditPanel('parteditor', 'body-editor-controls-container', [ [
                                                                                                            [ { id: "posLabel", name: AppResources.get( "pos_label" ) }, { id: "posX", name: "x", inputType: "float" },  { id: "posY", name: "y", inputType: "float" }, { id: "posZ", name: "z", inputType: "float" } ],
                                                                                                            [ { id: "rotLabel", name: AppResources.get( "rot_label" ) },  { id: "rotX", name: "x", inputType: "float" }, { id: "rotY", name: "y", inputType: "float" }, { id: "rotZ", name: "z", inputType: "float" } ],
                                                                                                            [ { id: "scaleLabel", name: AppResources.get( "scale_label" ) },  { id: "scaleX", name: "x", inputType: "float" }, { id: "scaleY", name: "y", inputType: "float" }, { id: "scaleZ", name: "z", inputType: "float" } ],
                                                                                                            [ { id: "vcolLabel", name: AppResources.get( "vcol_label" ) }, { id: "vcolR", name: "r", inputType: "float" }, { id: "vcolG", name: "g", inputType: "float" }, { id: "vcolB", name: "b", inputType: "float" } ],
  ],

                                                                                                          [
                                                                                                            [ { id: "shape", name: AppResources.get( "shape_label" ), inputType: "enum", options: partShapes } ]
                                                                                                          ],

                                                                                                          [
                                                                                                            [ { id: "mass", name: AppResources.get( "mass_label" ), inputType: "float" },  { id: "size", name: AppResources.get( "size_label" ), inputType: "float" } ],
                                                                                                            [ { id: "density", name: AppResources.get( "density_label" ), inputType: "float" }, { id: "friction", name: AppResources.get( "friction_label" ), inputType: "float" } ],
                                                                                                            [ { id: "ingest", name: AppResources.get( "ingest_label" ), inputType: "float" }, { id: "assim", name: AppResources.get( "assim_label" ), inputType: "float" } ],
                                                                                                            [ { id: "hollow", name: AppResources.get( "hollow_label" ), inputType: "float" } ],
                                                                                                          ]
                                                                                                      ]);

    var jointPropertiesEditPanel = new PropertiesEditPanel('jointeditor', 'body-editor-controls-container',  [ [
                                                                                                                [ { id: "posDeltaLabel", name: AppResources.get( "posDelta_label" ) },  { id: "posDeltaX", name: "x", inputType: "float" }, { id: "posDeltaY", name: "y", inputType: "float" }, { id: "posDeltaZ", name: "z", inputType: "float" } ],
                                                                                                                [ { id: "rotDeltaLabel", name: AppResources.get( "rotDelta_label" ) },  { id: "rotDeltaX", name: "x", inputType: "float" }, { id: "rotDeltaY", name: "y", inputType: "float" }, { id: "rotDeltaZ", name: "z", inputType: "float" } ],
                                                                                                                [ { id: "vcolLabel", name: AppResources.get( "vcol_label" ) }, { id: "vcolR", name: "r", inputType: "float" }, { id: "vcolG", name: "g", inputType: "float" }, { id: "vcolB", name: "b", inputType: "float" } ],
                                                                                                             ],

                                                                                                             [
                                                                                                               [ { id: "shape", name: AppResources.get( "shape_label" ), inputType: "enum", options: jointShapes } ]
                                                                                                             ],

                                                                                                             [
                                                                                                               [ { id: "stamina", name: AppResources.get( "stamina_label" ), inputType: "float" },  { id: "stif", name: AppResources.get( "stif_label" ), inputType: "float" } ],
                                                                                                               [ { id: "rotstif", name: AppResources.get( "rotstif_label" ), inputType: "float" },  { id: "usedelta", name:  AppResources.get( "usedelta_label" ), inputType: "boolean" } ],
                                                                                                             ]
                                                                                                           ]);

    var sceneActionMenu = new ActionMenu( "scene-action-menu", "body-editor-controls-container", [ { id: "addPart", text: AppResources.get( "addPart_label" ) }, { id: "addPartOnGrid", text: AppResources.get( "addPartOnGrid_label" ) } ] );
    var focusedPartActionMenu = new ActionMenu( "focused-part-action-menu", "body-editor-controls-container", [ { id: "delete", text: AppResources.get( "deletePart_label" ) } ] );
    var focusedJointActionMenu = new ActionMenu( "focused-joint-action-menu", "body-editor-controls-container", [ { id: "delete", text: AppResources.get( "deleteJoint_label" ) } ] );
    var joinPartsActionMenu = new ActionMenu( "join-parts-action-menu", "body-editor-controls-container", [ { id: "join", text: AppResources.get( "join_label" ) } ] );

    var cameraMenu = new CameraMenu( "body-editor-controls-container", "camera-menu", config.canvas.id, camera, scene );

    var logConsole = new LogConsole( "logconsole-container", "logconsole" );

    var sceneMouseInput = new SceneMouseInput( renderer );

    var transformControl = new TransformControls( camera.getPerspectiveCamera(), renderer.domElement );
    transformControl.addEventListener('dragging-changed', function (event) {
      camera.getCameraControl().enabled = !event.value;
    });
    var bodyElementEditTools = new BodyElementEditTools( "body-editor-controls-container", scene, transformControl );

    var raycaster = new Raycaster(config.raycaster, scene, camera.getPerspectiveCamera());

    camera.getCameraControl().addEventListener( 'change', function() {
	// update() no longer present in three js
    //   transformControl.update();
      sceneActionMenu.close();
      focusedPartActionMenu.close();
      focusedJointActionMenu.close();
      joinPartsActionMenu.close();
    } );

  }

  if( !AppParams['camera-input'] ) {
    camera.getCameraControl().enabled = false;
  }

  if( AppParams['show-grid'] ) {

    var grid = new Grid( config.grid.size, config.grid.interval, config.grid.centerLineColor, config.grid.color, AppParams['show-axes'] );
    scene.add( grid );

  }

  var ambientLight = new THREE.AmbientLight( config.light.ambient.color );
  scene.add( ambientLight );


  var configLightTop = config.light.directional.top; //shorter alias
  var topDirectionalLight = new THREE.DirectionalLight( configLightTop.color, configLightTop.intensity );
  topDirectionalLight.position.set( configLightTop.positionX, configLightTop.positionY, configLightTop.positionZ );
  scene.add( topDirectionalLight );

  var configLightBottom = config.light.directional.bottom; //shorter alias
  var bottomDirectionalLight = new THREE.DirectionalLight( configLightBottom.color, configLightBottom.intensity );
  bottomDirectionalLight.position.set( configLightBottom.positionX, configLightBottom.positionY, configLightBottom.positionZ );
  scene.add( bottomDirectionalLight );

  var resizeInEditorMode;

  if( !AppParams['viewer-mode'] ) {

    resizeInEditorMode = function( width, height, offsetTop ) {

      renderer.getContext().canvas.width = width;
      renderer.getContext().canvas.height = height;
      camera.getPerspectiveCamera().aspect = width / height;
      camera.getPerspectiveCamera().updateProjectionMatrix();
      renderer.setSize( width, height );
      renderer.setPixelRatio( window.devicePixelRatio || 1 );

      sceneMouseInput.setOffsetTop( offsetTop );
      partPropertiesEditPanel.resize();
      jointPropertiesEditPanel.resize();
      sceneActionMenu.close();
      focusedPartActionMenu.close();
      focusedJointActionMenu.close();
      joinPartsActionMenu.close();
      cameraMenu.setPosition( height );

    }

  } else {

    function resizeWindow ( event ) {

      renderer.getContext().canvas.width = window.innerWidth;
      renderer.getContext().canvas.height = window.innerHeight;
      camera.getPerspectiveCamera().aspect = window.innerWidth / window.innerHeight;
      camera.getPerspectiveCamera().updateProjectionMatrix();
      renderer.setSize( window.innerWidth, window.innerHeight );
      renderer.setPixelRatio( window.devicePixelRatio || 1 );

    }

    $( window ).resize( resizeWindow );

  }

  function render() {
    camera.getCameraControl().update();

    requestAnimationFrame(render);
    scene.render(renderer, camera.getPerspectiveCamera());
  }

  function clearView() {
    scene.clear();

    if( !AppParams['viewer-mode'] ) {
      raycaster.clear();
      bodyElementEditTools.close();
      partPropertiesEditPanel.hide();
      jointPropertiesEditPanel.hide();
      sceneActionMenu.close();
      focusedPartActionMenu.close();
      focusedJointActionMenu.close();
      joinPartsActionMenu.close();
    }

  }

  function updateBody( model ) {

    clearView();

    var meshes = [];

    var partMeshFactory = new PartMeshFactory( partShapes );
    var pc = model.getPartCount();
    var parts = [];
    for(var i=0; i<pc; ++i) {
      var mesh = partMeshFactory.create(model.getPart(i));
      parts.push( mesh.userData );
      meshes.push( mesh );
      scene.add( mesh );
    }

    var jointMeshFactory = new JointMeshFactory( jointShapes );
    var jc = model.getJointCount();
    for(var i=0; i<jc; ++i) {
      var mesh = jointMeshFactory.create( model.getJoint(i), parts );
      meshes.push( mesh );
      scene.add( mesh );
    }

    if( model.isNewGenotype ) {
      camera.zoomAll( meshes );
    }

	if(!cameraMenu){
		return undefined;
	}

    if( meshes.length > 0 ) {
      cameraMenu.showZoomAllBtn();
    } else {
      cameraMenu.hideZoomAllBtn();
    }

  }

  function updateMessagesInfo ( messages ) {
    logConsole.update( messages );
  }

  if( !AppParams['viewer-mode'] ) {

    controller( raycaster
      , scene
      , partPropertiesEditPanel
      , jointPropertiesEditPanel
      , sceneActionMenu
      , focusedPartActionMenu
      , focusedJointActionMenu
      , joinPartsActionMenu
      , bodyElementEditTools
      , sceneMouseInput
      , cameraMenu );

    AppEvents.register( "modelLogReaded", updateMessagesInfo );
  }

  AppEvents.register( "genotypeChanged_bodyData", updateBody );
  render();

  return {
    resizeInEditorMode: resizeInEditorMode
  }

})(FredConfig.bodyEditor, FramstickBodyEditorController, Framsticks.getPartShapes(), Framsticks.getJointShapes());
