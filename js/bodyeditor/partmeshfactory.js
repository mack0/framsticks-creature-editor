function PartMeshFactory(partShapes)
{
  var config = FredConfig.bodyEditor.geometry.part;

  function getNewSphereMesh(part, sphereRadius, segments, applyScale) {
    var geometry = new THREE.SphereGeometry( sphereRadius, segments, segments );
    var material = Transformations.getNewMaterial( part.get_vcolor().get_x(), part.get_vcolor().get_y(), part.get_vcolor().get_z() );
    var mesh = new THREE.Mesh( geometry, material );
    mesh.position.set(part.get_p().get_x(), part.get_p().get_y(), part.get_p().get_z());
    if(applyScale) {
      mesh.scale.set(part.get_scale().get_x(), part.get_scale().get_y(), part.get_scale().get_z());
    }

    var angles = part.get_o().getAngles();
    mesh.rotation.copy( Transformations.getPartRotation( angles.get_x(), angles.get_y(), angles.get_z() ) );

    mesh.userData = { isBodyElement: true, type: 'p', data: part, mesh: mesh, connectedJoints: [] };
    return mesh;
  }

  function getNewBoxMesh(part) {
    var geometry = new THREE.BoxGeometry(2, 2, 2);
    var material = Transformations.getNewMaterial( part.get_vcolor().get_x(), part.get_vcolor().get_y(), part.get_vcolor().get_z() );
    var mesh = new THREE.Mesh( geometry, material );
    mesh.position.set(part.get_p().get_x(), part.get_p().get_y(), part.get_p().get_z());
    mesh.scale.set(part.get_scale().get_x(), part.get_scale().get_y(), part.get_scale().get_z());

    var angles = part.get_o().getAngles();
    mesh.rotation.copy( Transformations.getPartRotation( angles.get_x(), angles.get_y(), angles.get_z() ) );

    mesh.userData = { isBodyElement: true, type: 'p', data: part, mesh: mesh, connectedJoints: [] };
    return mesh;
  }

  function getNewCylinderMesh(part) {
    var geometry = new THREE.CylinderGeometry(1, 1, 2, 32);
    var material = Transformations.getNewMaterial( part.get_vcolor().get_x(), part.get_vcolor().get_y(), part.get_vcolor().get_z() );
    var mesh = new THREE.Mesh( geometry, material );
    mesh.position.set(part.get_p().get_x(), part.get_p().get_y(), part.get_p().get_z());
    mesh.scale.set(part.get_scale().get_y(), part.get_scale().get_x(), part.get_scale().get_z());

    var angles = part.get_o().getAngles();
    var m = Transformations.getCylinderPartRotationMatrix( angles.get_x(), angles.get_y(), angles.get_z() );
    mesh.rotation.setFromRotationMatrix( m );

    mesh.userData = { isBodyElement: true, type: 'p', data: part, mesh: mesh, connectedJoints: [] };
    return mesh;
  }

  function create(part) {

    var shape = part.get_shape();
    if( partShapes['SHAPE_ELLIPSOID'].value == shape ) {
      return getNewSphereMesh(part, config.ellipsoidShape.radius, config.ellipsoidShape.segments, true);
    }
    else if( partShapes['SHAPE_CUBOID'].value == shape ) {
      return getNewBoxMesh(part);
    }
    else if( partShapes['SHAPE_CYLINDER'].value == shape ) {
      return getNewCylinderMesh(part);
    }

    return getNewSphereMesh(part, config.defaultShape.radius, config.defaultShape.segments, false);
  }

  return {
    create: create
  };
}
