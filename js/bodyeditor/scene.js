var Scene = function () {

  var scene = new THREE.Scene();

  this.add = function( obj ) {
    scene.add( obj );
  };

  this.remove = function( mesh ) {
    scene.remove( mesh );

    if( mesh.geometry ) {
      mesh.geometry.dispose();
      mesh.geometry = null;
    }
    if( mesh.material ) {
      mesh.material.dispose();
      mesh.material = null; 
    }
  };

  this.getChildren = function() {
    return scene.children;
  };

  this.getMeshes = function() {
    var meshes = [];
    for( var i=0; i<scene.children.length; ++i ) {
      var child = scene.children[ i ];
      if( child instanceof THREE.Mesh ) {
        meshes.push( child );
      }
    }
    return meshes;
  };

  this.clear = function() {
    var toRemove = [];
    for(var i=0; i<scene.children.length; ++i) {
      var obj = scene.children[i];
      if( obj.userData && obj.userData.isBodyElement ) {
        toRemove.push(obj);
      }
    }
    for(var i=0; i<toRemove.length; ++i) {
      this.remove(toRemove[i]);
    }
  };

  this.render = function(renderer, camera) {
    renderer.render(scene, camera)
  };

};
