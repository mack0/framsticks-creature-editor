var Transformations = (function () {

  var partShapes = Framsticks.getPartShapes();
  var jointShapes = Framsticks.getJointShapes();
  var config = FredConfig.bodyEditor.geometry.material;

  function getAffectedElements_r( bodyElement, result ) {
    if( bodyElement.marked ) {
      return;
    }

    if ( bodyElement.type == "p" ) {

      bodyElement.marked = true;
      result.partElements.push( bodyElement );

      for(var i=0; i<bodyElement.connectedJoints.length; ++i) {
        var joint = bodyElement.connectedJoints[ i ];
        if( !joint.marked ) {
          joint.marked = true;
          result.jointElements.push( joint );
          if( joint.data.get_usedelta() ) {
            getAffectedElements_r( joint.connectedParts[1], result );
          }
        }
      }

    }
    else if ( bodyElement.type == "j" ) {
      bodyElement.marked = true;
      result.jointElements.push( bodyElement );

      getAffectedElements_r( bodyElement.connectedParts[1], result );
    }
  }

  function getAffectedElements( bodyElement ) {
    var result = {
      partElements: [],
      jointElements: []
    };

    getAffectedElements_r( bodyElement, result );

    for(var j=0; j<result.partElements.length; ++j) {
      result.partElements[j].marked = false;
    }
    for(var k=0; k<result.jointElements.length; ++k) {
      result.jointElements[k].marked = false;
    }
    return result
  }

  function calcColorComponent(value) {
    return THREE.MathUtils.clamp(value, 0, 1);
  }

  function applyColor(bodyElement, component, value) {
    bodyElement.mesh.material.color[component] = calcColorComponent(value);
  }

  function getNewMaterial(r, g, b) {
    var color = new THREE.Color(calcColorComponent(r), calcColorComponent(g), calcColorComponent(b)).convertSRGBToLinear ();
    return new THREE.MeshStandardMaterial( { color: color, roughness: config.roughness, metalness: config.metalness} );
  }

  function getPartRotation( rotAngleX, rotAngleY, rotAngleZ ) {
    return new THREE.Euler( rotAngleX, -rotAngleY, rotAngleZ, "ZYX" );
  }

  function getCylinderPartRotationMatrix( rotAngleX, rotAngleY, rotAngleZ ) {
    var rot = getPartRotation( rotAngleX, rotAngleY, rotAngleZ );

    var m = new THREE.Matrix4();
    m.makeRotationZ( THREE.MathUtils.degToRad(90) );

    var m2 = new THREE.Matrix4();
    m2.makeRotationFromEuler( rot );
    m2.multiply( m );
    return m2;
  }

  function applySinglePartRotation(bodyElement) {
    var shape = bodyElement.data.get_shape();
    var rot = bodyElement.data.get_o().getAngles();

    if( partShapes['SHAPE_CYLINDER'].value == shape ) {
      var m = getCylinderPartRotationMatrix( rot.get_x(), rot.get_y(), rot.get_z() );
      bodyElement.mesh.rotation.setFromRotationMatrix( m );
    }
    else {
      var r = getPartRotation( rot.get_x(), rot.get_y(), rot.get_z() );
      bodyElement.mesh.rotation.copy( r );
    }
  }


  function applyPartRotation(bodyElement) {

    var affectedElements = getAffectedElements(bodyElement);

    if( affectedElements.partElements.length > 1 ) {
      for ( var i=0; i<affectedElements.partElements.length; ++i ) {
        var part = affectedElements.partElements[ i ];
        part.mesh.position.x = part.data.get_p().get_x();
        part.mesh.position.y = part.data.get_p().get_y();
        part.mesh.position.z = part.data.get_p().get_z();
        applySinglePartRotation(part);
      }
      var jointMeshFactory = new JointMeshFactory( jointShapes );
      for ( var j=0; j<affectedElements.jointElements.length; ++j ) {
        var newJointMesh = jointMeshFactory.create( affectedElements.jointElements[ j ].data );
        changeBodyElementMesh( affectedElements.jointElements[ j ], newJointMesh )
      }
    } else {
      applySinglePartRotation(bodyElement);
    }

  }


  function applyJointDeltaChange(bodyElement) {

    var affectedElements = getAffectedElements(bodyElement);

    for ( var i=0; i<affectedElements.partElements.length; ++i ) {
      var part = affectedElements.partElements[ i ];
      part.mesh.position.x = part.data.get_p().get_x();
      part.mesh.position.y = part.data.get_p().get_y();
      part.mesh.position.z = part.data.get_p().get_z();
      applySinglePartRotation(part);
    }
    var jointMeshFactory = new JointMeshFactory( jointShapes );
    for ( var j=0; j<affectedElements.jointElements.length; ++j ) {
      var newJointMesh = jointMeshFactory.create( affectedElements.jointElements[ j ].data );
      changeBodyElementMesh( affectedElements.jointElements[ j ], newJointMesh )
    }

  }

  function applyPartScale(bodyElement) {
    var part = bodyElement.data;
    var shape = part.get_shape();
    if( partShapes['SHAPE_CYLINDER'].value == shape ) {
      bodyElement.mesh.scale.set(part.get_scale().get_y(), part.get_scale().get_x(), part.get_scale().get_z());
    } else if ( partShapes['SHAPE_BALL'].value != shape ) {
      bodyElement.mesh.scale.set(part.get_scale().get_x(), part.get_scale().get_y(), part.get_scale().get_z());
    }
  }

  function changeBodyElementMesh( element, newMesh ) {
    var scene = element.mesh.parent;
    var oldMesh = element.mesh;

    var userData = oldMesh.userData;
    userData.showTransparent = newMesh.userData.showTransparent;
    userData.mesh = newMesh.userData.mesh;
    newMesh.userData = userData;

    scene.remove(oldMesh);
    scene.add(newMesh);

    oldMesh.geometry.dispose();
    oldMesh.geometry = null;
    oldMesh.material.dispose();
    oldMesh.material = null;
  }


  function applyPartPosition(bodyElement, axis) {
    var jointMeshFactory = new JointMeshFactory( jointShapes );

    var affectedElements = getAffectedElements(bodyElement);
    for ( var i=0; i<affectedElements.partElements.length; ++i ) {
      affectedElements.partElements[ i ].mesh.position[axis] = affectedElements.partElements[ i ].data.get_p()["get_" + axis]();
    }
    for ( var j=0; j<affectedElements.jointElements.length; ++j ) {
      var newJointMesh = jointMeshFactory.create( affectedElements.jointElements[ j ].data );
      changeBodyElementMesh( affectedElements.jointElements[ j ], newJointMesh )
    }
  }


  function updateAfterPartTranslation(bodyElement) {
    var jointMeshFactory = new JointMeshFactory( jointShapes );

    var affectedElements = getAffectedElements(bodyElement);
    for ( var i=0; i<affectedElements.partElements.length; ++i ) {
      var currentElement = affectedElements.partElements[ i ];
      if( currentElement !== bodyElement ) {
        affectedElements.partElements[ i ].mesh.position.x = affectedElements.partElements[ i ].data.get_p().get_x();
        affectedElements.partElements[ i ].mesh.position.y = affectedElements.partElements[ i ].data.get_p().get_y();
        affectedElements.partElements[ i ].mesh.position.z = affectedElements.partElements[ i ].data.get_p().get_z();
      }
    }
    for ( var j=0; j<affectedElements.jointElements.length; ++j ) {
      var newJointMesh = jointMeshFactory.create( affectedElements.jointElements[ j ].data );
      changeBodyElementMesh( affectedElements.jointElements[ j ], newJointMesh )
    }
  }

  function applyPartShape(bodyElement) {
    var partMeshFactory = new PartMeshFactory( partShapes );
    var newPartMesh = partMeshFactory.create( bodyElement.data );
    changeBodyElementMesh( bodyElement, newPartMesh )
  }

  function applyJointShape(bodyElement) {
    var jointMeshFactory = new JointMeshFactory( jointShapes );
    var newJointMesh = jointMeshFactory.create( bodyElement.data );
    changeBodyElementMesh( bodyElement, newJointMesh )
  }

  return {
    applyColor: applyColor,
    applyPartPosition: applyPartPosition,
    updateAfterPartTranslation: updateAfterPartTranslation,
    applyPartRotation: applyPartRotation,
    applyPartScale: applyPartScale,
    applyPartShape: applyPartShape,
    getNewMaterial: getNewMaterial,
    getPartRotation: getPartRotation,
    applyJointDeltaChange: applyJointDeltaChange,
    getCylinderPartRotationMatrix: getCylinderPartRotationMatrix,
    applyJointShape: applyJointShape
  };
})();
