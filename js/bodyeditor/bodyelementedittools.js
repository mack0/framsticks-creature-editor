var BodyElementEditTools = function( containerId, scene, transformControl ) {

  var config = FredConfig.bodyEditor.selectedElement;
  var elementWireframe;
  var element;

  var isMouseDown = false;
  var partPositionChangeIsBlocked = false;
  var handlers = {
    ontranslate: null,
    ontranslated: null
   }

  function init( ) {

    setTransformationMode( "translate" );
    transformControl.addEventListener( "change", handleChange );
    transformControl.addEventListener( "mouseDown", handleMouseDown );
    transformControl.addEventListener( "mouseUp", handleMouseUp );
    scene.add( transformControl );

  }

  function setTransformationMode( mode ) {
    transformControl.setMode( mode );
    transformControl.setSpace( "world" );
  }

  function handleChange ( event ) {

      //var mode = transformControl.getMode();
      //if( mode === "translate" ) {

        if( typeof handlers.ontranslate === "function" ) {
          if( isMouseDown ) {
            handlers.ontranslate( element );
          }
        }
    //  }
  }
  function handleMouseDown ( event ) {
    isMouseDown = true;
  }
  function handleMouseUp  ( event ) {
    isMouseDown = false;
    if( typeof handlers.ontranslated === "function" ) {
        handlers.ontranslated( element );
    }
  }

  function open( bodyElement ) {

    if( bodyElement === element ) {
      return;
    }

    close();

    if( bodyElement && bodyElement.mesh ) {
      element = bodyElement;
    makeWireframe(element);

      if( bodyElement.type === 'p' ) {
        partPositionChangeIsBlocked = false;
        for(var i=0; i<bodyElement.connectedJoints.length; ++i) {
          if( bodyElement.connectedJoints[i].connectedParts[0] !== bodyElement && bodyElement.connectedJoints[i].data.get_usedelta() ) {
            partPositionChangeIsBlocked = true;
            break;
          }
        }

        if( !partPositionChangeIsBlocked ) {
          transformControl.attach( bodyElement.mesh );
        }
      }

    }

  }

  function close() {

    if( elementWireframe && element ) {
      element.mesh.remove( elementWireframe );
      if( (element.type === 'p' && !partPositionChangeIsBlocked) ) {
        transformControl.detach( element );
      }
    }
    elementWireframe = null;
    element = null;

  }

  function getEventHandlers() {
    return handlers;
  }

  function update() {
	// update() no longer present in three js
    // transformControl.update();
  }

  function makeWireframe(element){
    const elementHelper = new THREE.EdgesGeometry( element.mesh.geometry );
    elementWireframe = new THREE.LineSegments(elementHelper, new THREE.LineBasicMaterial( { color: config.wireframeColor } ) ); 
    element.mesh.add(elementWireframe);
  }

  function updateAfterMeshChange() {
    if( elementWireframe && element ) {
      element.mesh.remove( elementWireframe );
    }

    makeWireframe(element);

    if( element.type === 'p' && !partPositionChangeIsBlocked ) {
      transformControl.attach( element.mesh );
	  // update() no longer present in three js
      // transformControl.update();
    }
  }

  function detachTransformControls() {
    if( element && element.mesh ) {
      transformControl.detach( element.mesh );
    }
  }

  init();

  return {
    open: open,
    close: close,
    getEventHandlers: getEventHandlers,
    update: update,
    updateAfterMeshChange: updateAfterMeshChange,
    detachTransformControls: detachTransformControls
  };

};
