function JointMeshFactory(jointShapes)
{

  var config = FredConfig.bodyEditor.geometry.joint;

  function getNewJointMesh( joint, shapeConfig ) {

    var firstPartPosVec  = new THREE.Vector3( joint.get_part1().get_p().get_x(), joint.get_part1().get_p().get_y(), joint.get_part1().get_p().get_z() );
    var secondPartPosVec = new THREE.Vector3( joint.get_part2().get_p().get_x(), joint.get_part2().get_p().get_y(), joint.get_part2().get_p().get_z() );
    var line = new THREE.LineCurve3(firstPartPosVec, secondPartPosVec);
    var geometry = new THREE.TubeGeometry(line, 1, shapeConfig.radius, shapeConfig.radiusSegments, false);

    var material = Transformations.getNewMaterial(joint.get_vcolor().get_x(), joint.get_vcolor().get_y(), joint.get_vcolor().get_z());
    material.transparent = shapeConfig.isTransparent;
    material.opacity = shapeConfig.opacity;

    var mesh = new THREE.Mesh( geometry, material );
    mesh.userData = { isBodyElement: true, type: 'j', data: joint, mesh: mesh, connectedParts: [], showTransparent: shapeConfig.isTransparent };
    return mesh

  }

  function addConnectionInfo( jointMesh, partMeshes ) {

    var p1 = jointMesh.data.get_part1();
    var p2 = jointMesh.data.get_part2();
    var count = 0;
    for(var i=0; i<partMeshes.length && count<2; ++i) {
      if( partMeshes[i].data === p1 || partMeshes[i].data === p2 ) {
        jointMesh.connectedParts.push( partMeshes[i] );
        partMeshes[i].connectedJoints.push( jointMesh );
        ++count;
      }
    }

  }

  function create(joint, partMeshes) {
    var result;
    var shape = joint.get_shape();

    if( jointShapes['SHAPE_STICK'].value == shape ) {
      result = getNewJointMesh( joint, config.cylinderShape );
    } else {
      result = getNewJointMesh( joint, config.linkShape );
    }

    if( partMeshes ) {
      addConnectionInfo( result.userData, partMeshes );
    }

    return result;
  }

  return {
    create: create
  };
}
