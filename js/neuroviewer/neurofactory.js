var NeuroFactory = function( config ) {

  var neuroClassHints = Framsticks.getNeuroClassHints();

  function create( x, y, w, h, neuroData ) {

    var neuroClass = neuroData.getClass();
    if( neuroClass.getVisualHints() === neuroClassHints["ReceptorClass"].value ) {
      return new ReceptorDrawableNeuron(  x, y, w, h, neuroData, config );
    }

    return new GenericDrawableNeuron(  x, y, w, h, neuroData, config );

  }

  return {
    create: create
  }
};
