var NeuroViewerView = (function( config )
{

  var objectsToDraw = [];
  var bounds = null;
  

  var hasSomethingToDraw = false;

  var container = document.getElementById( 'neuro-viewer-container' );
  container.style.height = `${config.canvas.heightPercentViewerMode}%`;

  var infoTextElement = document.createElement("div");
  infoTextElement.id = "neuro-info-text";
  infoTextElement.style.font = config.canvas.font;

  var canvas = document.createElement( 'canvas' );
  canvas.id = config.canvas.id;
  var context = canvas.getContext( '2d' );
  if(AppParams["show-nn"]){
	container.appendChild(infoTextElement);
    container.appendChild( canvas );
  }
  if(AppParams["viewer-mode"]){
	config.canvas.drawMarginInPx = config.canvas.drawMarginInPxInViewerMode
  }

  var screenWidth = canvas.width;
  var screenHeight = canvas.height;
  var devicePixelRatio = (window.devicePixelRatio || 1);
  var backingStoreRatio = context.webkitBackingStorePixelRatio ||
      context.mozBackingStorePixelRatio ||
      context.msBackingStorePixelRatio ||
      context.oBackingStorePixelRatio ||
      context.backingStorePixelRatio || 1;

  var screenRatio = devicePixelRatio / backingStoreRatio;

  function resize( width, height ) {
	if (AppParams["viewer-mode"]){
		if(bounds){
			container.style.height = '25%';
			width = (bounds.height!=0?(bounds.width/bounds.height):1)*height;
		}
		if (!hasSomethingToDraw){
			resizeText();
			container.style.width = "auto";
			container.style.height = "auto";
			container.style.minHeight = null;
		}
	}

    screenWidth = width;
    screenHeight = height;

    canvas.width = width * screenRatio;
    canvas.height = height * screenRatio;

    canvas.style.width = width + 'px';
    canvas.style.height = height + 'px';

    context.scale(screenRatio, screenRatio);

    render();
  }
  function resizeText() {
	const fontSize = Math.max(document.documentElement.clientHeight / 10, 7); // Minimum 7px, adjust with container size
	infoTextElement.style.fontSize = fontSize + "px";
  }

  function render() {
    context.clearRect(0, 0, canvas.width, canvas.height);

    if( !hasSomethingToDraw ) {
		if(!AppParams["inform-no-nn"]) return;

		if(AppParams["viewer-mode"]){
			canvas.style.display = "none";
			infoTextElement.style.display = "block";
			infoTextElement.textContent = AppResources.get("emptyNN_label");
		}else{
			context.font = config.canvas.font;
			context.fillStyle = config.canvas.strokeStyle;
			context.fillText( AppResources.get( "emptyNN_label" ), 12, 22 );
		}
		return;
    }

	infoTextElement.style.display = "none";
	canvas.style.display = "block";
    if( bounds != null ) {
      for( var i=0; i<objectsToDraw.length; ++i ) {
        objectsToDraw[i].update( screenWidth, screenHeight, bounds );
        objectsToDraw[i].draw( context );
      }
    }
  }

  function getNeuroLayoutBounds( data ) {

    var result = { minX: null, maxX: null, minY: null, maxY: null };

    for( var i=0; i<data.getElements(); i++ ) {

      var xywh=data.getValueXYWH(i);
      if( (result.minX == null) || (result.minX > xywh.get_x()) ) {
        result.minX = xywh.get_x();
      }
      var xw = xywh.get_x() + xywh.get_w();
      if( (result.maxX == null) || (result.maxX < xw)  ) {
        result.maxX = xw;
      }
      if( (result.minY == null) || (result.minY > xywh.get_y()) ) {
        result.minY = xywh.get_y();
      }
      var yh = xywh.get_y() + xywh.get_h();
      if( (result.maxY == null) || (result.maxY < yh)  ) {
        result.maxY = yh;
      }

    }

    result.width = Math.abs( result.maxX - result.minX );
    result.height = Math.abs( result.maxY - result.minY );
    return result;
  }

  function updateBrain( data ) {

    objectsToDraw = [];
    bounds = getNeuroLayoutBounds( data );

    var neuroFactory = new NeuroFactory( config.canvas );
    for( var i=0; i<data.getElements(); i++ ) {

      var xywh=data.getValueXYWH(i);
      var neuro = neuroFactory.create( xywh.get_x(), xywh.get_y(), xywh.get_w(), xywh.get_h(), data.getNeuro(i) );
      objectsToDraw.push( neuro );

      for( var j=0; j<data.getNeuro(i).getInputCount(); j++ ) {
        var inputRefNo = data.getNeuro(i).getInput(j).get_refno();
        var inputCoords = data.getValueXYWH(inputRefNo);

        neuro.inputsLayoutPositions.push( {
          x: inputCoords.get_x(),
          y: inputCoords.get_y(),
          w: inputCoords.get_w(),
          h: inputCoords.get_h()
        } );
      }
    }
	hasSomethingToDraw = objectsToDraw.length > 0;

	resize(screenWidth,screenHeight);
  }

  AppEvents.register( "genotypeChanged_brainData", updateBrain );
  AppEvents.register( "bodyElementRemoved_brainData", updateBrain );

  return {
    resize: resize
  }

})( FredConfig.neuroViewer );
