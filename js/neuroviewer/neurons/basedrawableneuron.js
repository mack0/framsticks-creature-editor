function BaseDrawableNeuron ( x, y, w, h, neuroData, config ) {

  this.drawCoords = null;
  this.inputsLayoutPositions = [];
  this.inputLinkDrawPositions = [];
  this.neuroData = neuroData;

  this.layoutCoords = {
    x: x,
    y: y,
    w: w,
    h: h
  };

  this.calcConnectionLineCoords = function () {

    return {
      inputEndLineX: this.drawCoords.x + this.drawCoords.w * 0.2,
      outputBeginLineX: this.drawCoords.x + this.drawCoords.w * 0.8,
      outputLineY: this.drawCoords.y + this.drawCoords.h * 0.5
    }

  }

  this.drawInputLines = function ( context, inputEndLineX ) {

    var inputCount = neuroData.getInputCount();
    for ( var i = 0; i < inputCount; ++i ) {

      var inputH = (1 / (inputCount + 1) * this.drawCoords.h);
      var inputLineY = this.drawCoords.y + inputH * (i + 1);
      context.beginPath();
      context.moveTo( inputEndLineX, inputLineY );
      context.lineTo( this.drawCoords.x, inputLineY );
      context.lineTo( this.inputLinkDrawPositions[ i ].x, this.inputLinkDrawPositions[ i ].y );
      context.stroke();

    }

  }

  this.update = function ( canvasWidth, canvasHeight, bounds ) {

    var offset = config.drawMarginInPx * 2;

    var scaleW = (canvasWidth - offset) / bounds.width;
    var scaleH = (canvasHeight - offset) / bounds.height;
    var scale = Math.min( scaleW, scaleH );

    var posX = (Math.abs(this.layoutCoords.x - bounds.minX) / bounds.width) * scale * bounds.width;
    var posY = (Math.abs(this.layoutCoords.y - bounds.minY) / bounds.height) * scale * bounds.height;

    this.drawCoords = {
      x: posX + config.drawMarginInPx,
      y: posY + config.drawMarginInPx,
      w: this.layoutCoords.w * scale,
      h: this.layoutCoords.h * scale
    }

    this.inputLinkDrawPositions = [];
    for ( var i = 0; i < this.inputsLayoutPositions.length; ++i ) {

      var inputX = (Math.abs( this.inputsLayoutPositions[ i ].x - bounds.minX ) / bounds.width) * scale * bounds.width;
      var inputY = (Math.abs( this.inputsLayoutPositions[ i ].y - bounds.minY ) / bounds.height) * scale * bounds.height;

      this.inputLinkDrawPositions[ i ] = {
        x: inputX + this.inputsLayoutPositions[ i ].w * scale + config.drawMarginInPx,
        y: inputY + this.inputsLayoutPositions[ i ].h * 0.5 * scale + config.drawMarginInPx,
      }

    }

  }

  this.draw = function ( context ) {

    if ( this.drawCoords != null ) {
      context.strokeStyle = config.strokeStyle;

      var connCoords = this.calcConnectionLineCoords();
      this.drawInputLines( context, connCoords.inputEndLineX );

      if( typeof this.drawNeuron === "function" ) {
        this.drawNeuron( context, connCoords );
      }

    }

  }

};