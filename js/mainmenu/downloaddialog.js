function DownloadDialog ( containerId ) {

  var dialog;
  var downloadHandler;
  var inputField;

  (function init() {

    var container = document.getElementById( containerId );
    if( container ) {

      var form = document.createElement( 'form' );
      var fieldset = document.createElement( 'fieldset' );
      form.appendChild( fieldset );

      var nameFieldDiv = document.createElement( 'div' );
      nameFieldDiv.classList.add( "dialog-field" );

      var nameLabel = document.createElement( 'label' );
      nameLabel.htmlFor = "name";
      nameLabel.textContent = AppResources.get( "name_label" );
      nameFieldDiv.appendChild( nameLabel );

      var nameInput = document.createElement( 'input' );
      nameInput.id = "downloadfile-name-input";
      nameInput.type = "text";
      nameInput.name = "name";
      nameInput.onblur = handleInputBlur;
      nameInput.classList.add( "text" );
      nameInput.classList.add( "ui-widget-content" );
      nameInput.classList.add( "ui-corner-all" );
      nameInput.classList.add( "dialog-textinput" );

      inputField = nameInput;

      nameFieldDiv.appendChild( nameInput );

      fieldset.appendChild( nameFieldDiv );

      container.appendChild( form );

      dialog = $( '#' + containerId ).dialog( {
        title: AppResources.get( "downloadDialog_title" ),
        autoOpen: false,
        height: 165,
        width: 250,
        modal: true,
        buttons: {
          Download: function () {
            if ( typeof downloadHandler === "function" ) {
              downloadHandler();
            }
            dialog.dialog( "close" );
          },
          Close: function () {
            dialog.dialog( "close" );
          }
        }
      } );
    }

  }());

  function reloadInputValue( ) {
    if( typeof inputField.onvalueload === "function" ) {
      inputField.onvalueload( inputField );
    }
  }

  function handleInputBlur(event) {
    if( event && event.target ) {
      if ( typeof inputField.onvaluechange === "function" ) {
        inputField.onvaluechange( inputField.value );
      }
    }
  }

  this.open = function( ) {

    reloadInputValue( );
    dialog.dialog( "open" );

  };

  this.setDownloadHandler = function( handler ) {
    downloadHandler = handler;
  };

  this.getNameInput = function( ) {
    return inputField;
  }

};
