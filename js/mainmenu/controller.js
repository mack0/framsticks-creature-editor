var MainMenuController = function ( view, aboutDialog, downloadDialog ) {

  var fileHandler = new FileHandler();

  view.getHandlerByOptionId( "file-new" ).onmenuitemaction = function ( e ) {

    var result = window.confirm( AppResources.get( "loadStartupFile_question" ) );
    if( result ) {
      Framsticks.loadCreatureAsNewFile( FredConfig.startupFile );
      fileHandler.fileName = FredConfig.fileHandler.defaultFileName;
    }

  }

  view.getHandlerByOptionId( "file-load" ).onmenuitemaction = function ( e ) {

    fileHandler.readDataFromFile( e );

  }

  view.getHandlerByOptionId( "file-save" ).onmenuitemaction = function ( e ) {

    downloadDialog.open();

  }

  view.getHandlerByOptionId( "edit-undo" ).onmenuitemaction = function ( e ) {

    AppEvents.publish("undo");

  }

  view.getHandlerByOptionId( "edit-redo" ).onmenuitemaction = function ( e ) {

    AppEvents.publish("redo");

  }

  view.getHandlerByOptionId( "content-manage" ).onmenuitemaction = function ( e ) {

    AppEvents.publish( 'openCreatureListDialog' );

  }

  view.getHandlerByOptionId( "content-details" ).onmenuitemaction = function ( e ) {

    AppEvents.publish( 'openCreatureDetailsDialog' );

  }

  view.getHandlerByOptionId( "help-guide" ).onmenuitemaction = function ( e ) {

    window.open( "user_guide.html", "_blank" );

  }


  view.getHandlerByOptionId( "help-about" ).onmenuitemaction = function ( e ) {

    aboutDialog.open();

  }

  downloadDialog.getNameInput().onvalueload = function( input ) {
    input.value = fileHandler.fileName;
  }

  downloadDialog.getNameInput().onvaluechange = function( value ) {
    fileHandler.fileName = value;
  }

  downloadDialog.setDownloadHandler( function downloadFileHandler() {
    fileHandler.downloadFile()
  } );

};
