var CreatureListDialog = function ( containerId, id, width, height ) {

  var creatureListDiv;
  var dialog;
  var openedInfoLabel;

  var handlers = {
    onAddNewCreature: null,
    onDeleteCreature: null,
    onOpenCreature: null
  };

  function init() {

    var container = document.getElementById( containerId );
    if( container ) {

      var addBtnContainer = document.createElement( 'div' );
      addBtnContainer.id = "addbtn-container";
      container.appendChild( addBtnContainer );

      $( '#addbtn-container' ).button( {
        icons: { primary: "ui-icon-plus" },
        label: AppResources.get( "add_label" ) }
      ).click( addNewCreature );

      openedInfoLabel = document.createElement( 'div' );
      openedInfoLabel.classList.add( "creaturelist-textcontent" );
      openedInfoLabel.classList.add( "creaturelist-openedinfolabel" );
      container.appendChild( openedInfoLabel );

      creatureListDiv = document.createElement( 'div' );
      creatureListDiv.id = id;
      creatureListDiv.className = "creaturelist";
      container.appendChild( creatureListDiv );

      dialog = $( '#'+containerId ).dialog({
        autoOpen: false,
        height: height,
        width: width,
        modal: true
      });
    }

  }

  function parseCreatureID( event ) {
    if( event && event.currentTarget && event.currentTarget.id ) {
      var result = event.currentTarget.id.split( '-' );
      if( result.length > 1 ) {
        var result = parseInt( result[ result.length-1 ] );
        if( !isNaN(result) && result > -1 ) {
          return result;
        }
      }
    }
  }

  function openCreature( event ) {
    var creatureID = parseCreatureID( event );
    if( typeof handlers.onOpenCreature === 'function' && creatureID ) {
      handlers.onOpenCreature( creatureID );
    }
    dialog.dialog( "close" );
  }

  function deleteCreature( event ) {
    var creatureID = parseCreatureID( event );
    if( typeof handlers.onDeleteCreature === 'function' && creatureID ) {
      handlers.onDeleteCreature( creatureID );
    }
  }

  function addNewCreature() {
    if( typeof handlers.onAddNewCreature === 'function' ) {
      handlers.onAddNewCreature();
    }
  }

  function clear() {
    while ( creatureListDiv.firstChild ) {
      creatureListDiv.removeChild( creatureListDiv.firstChild );
    }
  }

  function getNewCellDiv( className ) {
    var cell = document.createElement( 'div' );
    cell.classList.add( "creaturelist-row-cell" );
    cell.classList.add( className );
    return cell;
  }

  function getNewCellTextContentDiv( text ) {
    var content = document.createElement( 'div' );
    content.classList.add( "creaturelist-textcontent" );
    content.textContent = text;
    return content;
  }

  function getCreatureRow( creature, showDeleteButton, rowNum ) {

    var creatureRow = document.createElement( 'div' );
    creatureRow.classList.add( "creaturelist-row" );
    creatureListDiv.appendChild( creatureRow );

    var rowNumCell = getNewCellDiv( "creaturelist-rownumcell" );
    var rowNumContent = getNewCellTextContentDiv( rowNum );
    rowNumCell.appendChild( rowNumContent );
    creatureRow.appendChild( rowNumCell );

    var nameCell = getNewCellDiv( "creaturelist-namecell" );
    var nameContent = getNewCellTextContentDiv( creature.name );
    nameCell.appendChild( nameContent );
    creatureRow.appendChild( nameCell );

    if( showDeleteButton ) {

      var deleteCell = getNewCellDiv( "creaturelist-deletecell" );
      var deleteBtn = document.createElement( 'div' );
      deleteBtn.id = "creaturelist-deletebtn-" + String( creature.id );
      deleteCell.appendChild( deleteBtn );
      creatureRow.appendChild( deleteCell );

      $( '#'+deleteBtn.id ).button( {
        icons: { primary: "ui-icon-trash" },
        label: AppResources.get( "delete_label" )
      } ).click( deleteCreature );

    }

    var openCell = getNewCellDiv( "creaturelist-opencell" );
    var openBtn = document.createElement( 'div' );
    openBtn.id = "creaturelist-openbtn-" + String( creature.id );

    openCell.appendChild( openBtn );
    creatureRow.appendChild( openCell );

    $( '#'+openBtn.id ).button( {
      icons: { primary: "ui-icon-folder-open" },
      label: AppResources.get( "open_label" )
    } ).click( openCreature );

    return creatureRow;

  }

  function loadData( creaturesData ) {

    clear();

    var currentCreature = creaturesData.currentCreature;
    var creatures = creaturesData.creatures;

    var currentCreatureRowNum;

    for( var i=0; i<creatures.length; ++i ) {

      var rowNum = i+1

      if( currentCreature && currentCreature.id == creatures[ i ].id ) {
        currentCreatureRowNum = rowNum;
      }

      var creatureRow = getCreatureRow( creatures[ i ], (creatures.length > 1), rowNum );
      creatureListDiv.appendChild( creatureRow );

    }

    setMode( currentCreature, currentCreatureRowNum );

  }

  function setMode( currentCreature, currentCreatureRowNum ) {

    var isCreatureOpened = (currentCreature && currentCreatureRowNum);
    dialog.dialog( 'option', 'closeOnEscape', isCreatureOpened );

    if( isCreatureOpened ) {

      dialog.dialog( 'option', 'buttons', {
        Close: function() {
          dialog.dialog( "close" );
        }
      });

      dialog.dialog( 'option', 'title', AppResources.get( "creatureListDialogManage_title" ) );

      if( currentCreature.name ) {
        openedInfoLabel.textContent = AppResources.get( "namedCreatureOpened_label", [ currentCreatureRowNum, currentCreature.name ] );
      } else {
        openedInfoLabel.textContent = AppResources.get( "creatureOpened_label", [ currentCreatureRowNum ] );
      }

    } else {

      dialog.dialog( 'option', 'buttons', { } );
      dialog.dialog( 'option', 'title', AppResources.get( "creatureListDialogOpen_title" ) );
      openedInfoLabel.textContent = "";
    }

  }

  function open( creaturesData ) {

    loadData( creaturesData );

    dialog.dialog( "open" );

  }

  function getHandlers() {
    return handlers;
  }

  init();

  return {
    open: open,
    getHandlers: getHandlers,
    loadData: loadData
  };

};
