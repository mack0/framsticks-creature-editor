var AppResources = function() {

  var resources = [];
  resources["addPartOnGrid_error"] = "Mouse cursor is not pointing on the grid surface.";
  resources["undo_error"] = "There is nothing to undo.";
  resources["redo_error"] = "There is nothing to redo.";
  resources["fileApiNotSupported_error"] = "Error: The File APIs are not fully supported by your browser.";
  resources["fileLoading_error"] = "Error: Cannot load file correctly.";
  resources["fileTooLarge_error"] = "Error: File is too large. Maximal size is {0}MB."
  resources["invalidFileData_error"] = "Error: Application cannot read valid framstick model from this file :(";

  resources["partDelete_question"] = "Are you sure you want to delete the selected Part?";
  resources["jointDelete_question"] = "Are you sure you want to delete the selected Joint?";
  resources["creatureDelete_question"] = "Are you sure you want to delete selected creature?";
  resources["loadStartupFile_question"] = "This will load startup file overriding your current creatures data.\nAre you sure you want to do this?";

  resources["fileNew_label"] = "New";
  resources["fileLoad_label"] = "Load";
  resources["fileSave_label"] = "Save";
  resources["editUndo_label"] = "Undo";
  resources["editRedo_label"] = "Redo";
  resources["contentManage_label"] = "Manage creatures in file";
  resources["contentDetails_label"] = "Edit creature details";
  resources["helpGuide_label"] = "Help";
  resources["helpAbout_label"] = "About";

  resources["add_label"] = "Add";
  resources["delete_label"] = "Delete";
  resources["open_label"] = "Open";

  resources["name_label"] = "Name";
  resources["info_label"] = "Info";

  resources["pos_label"] = "Position";
  resources["rot_label"] = "Rotation";
  resources["scale_label"]= "Scale";
  resources["vcol_label"] = "Vcolor";
  resources["shape_label"] = "Shape";
  resources["mass_label" ] = "Mass";
  resources["size_label"] = "Size";
  resources["density_label"] = "Density";
  resources["friction_label"] = "Friction";
  resources["ingest_label"] = "Ingestion";
  resources["assim_label"] = "Assimilation";
  resources["hollow_label"] = "Hollow";
  resources["posDelta_label"] = "Position delta";
  resources["rotDelta_label"] = "Rotation delta";
  resources["stamina_label"] = "Stamina";
  resources["stif_label"] = "Stiffness";
  resources["rotstif_label"] = "Rotation stiffness";
  resources["usedelta_label"] = "Use delta";
  resources["addPart_label"] = "Add new part here" ;
  resources["addPartOnGrid_label"] = "Add new part on grid";
  resources["deletePart_label"] = "Delete Part";
  resources["deleteJoint_label"] = "Delete Joint";
  resources["join_label"] = "Join parts";
  resources["creatureOpened_label"] = "Creature no. {0} is now opened.";
  resources["namedCreatureOpened_label"] = "Creature no. {0} named \"{1}\" is now opened.";

  resources["emptyNN_label"] = "No neural network";
  resources["validModel_label"] = "No issues were found in current model";
  resources["invalidModel_label"] = "Found {0} issue(s) in current model";

  resources["zoomAll_label"] = "Zoom all";

  resources["creatureListDialogManage_title"] = "Manage creature collection";
  resources["creatureListDialogOpen_title"] = "Open creature to edit";
  resources["creatureDetailsDialog_title"] = "Edit creature details";
  resources["aboutDialog_title"] = "About Framsticks Creature Editor";
  resources["downloadDialog_title"] = "Input file name";

  resources["aboutDialog_content"] =  "<p>This editor uses Framsticks SDK:<br>"
    + "<a href='http://www.framsticks.com/sdk' target='_blank'>http://www.framsticks.com/sdk</a></p>"
    + "<p>This application is released under the Creative Commons <a href='https://creativecommons.org/licenses/by-sa/4.0/legalcode' target='_blank'>CC-BY-SA</a> license.</p>"
    + "<p>Credits:</p>"
    + "<ul>"
    + "<li>Bartosz Michalski (<span id='about-author'></span>): programming and testing this app</li>"
    + "<li>Maciej Komosinski (<a href='mailto:support@framsticks.com'>email</a>): concept, mentoring, testing, Framsticks SDK</li>"
    + "<li>Szymon Ulatowski: Framsticks SDK</li>"
    + "<li>Konrad Miazga: testing</li>"
	+ "<li>Łukasz Andryszewski: updates</li>"
    + "</ul>"
    + "<p>Issue tracker and sources:<br>"
    + "<a href='https://bitbucket.org/mack0/framsticks-creature-editor' target='_blank'>https://bitbucket.org/mack0/framsticks-creature-editor</a></p>";

  function get( name, args ) {
    return stringFormat( resources[ name ], args );
  }

  return {
    get: get
  }

}();