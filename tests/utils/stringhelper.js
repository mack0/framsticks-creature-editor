console.log( stringFormat( "Framsticks are fun!" ) );
console.log( stringFormat( "Framsticks {0}!", [ "are fun" ] ) );
console.log( stringFormat( "{0} {1}{2}", [ "Framsticks", "are fun", "!" ] ) );
console.log( stringFormat( "{2} {1}{0}", [ "!", "are fun", "Framsticks" ] ) );