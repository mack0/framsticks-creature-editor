console.log("***** use delta *****");
/* Init */
var logger = new Module.LoggerToMemory(Module.LoggerBase["Enable"] + Module.LoggerToMemory["StoreAllMessages"]);
var validators = new Module.Validators();
var g1 = new Module.Geno().useValidators(validators);
var modelValidator = new Module.ModelGenoValidator();
validators.append(modelValidator);
Module.destroy(g1);
/* End Init*/

var resultGeno = new Module.Geno();
var testGeno = "//0\np:\np:\nj:0,1\n";

var str = new Module.SString();
str.set(testGeno);

var g2 = new Module.Geno(str);
var m = new Module.Model(g2, true);

console.log( "Output after model load." );
m.makeGeno(resultGeno); console.log(resultGeno.getGenes().c_str());
m.validate();           console.log( logger.getMessages().c_str() );
logger.reset();

m.open(); console.log( "m.open()" );

var j0 = m.getJoint(0);
j0.set_usedelta(true);
m.validate();

m.makeGeno(resultGeno);
console.log(resultGeno.getGenes().c_str());

//j0.set_usedelta(false);
m.validate();

m.makeGeno(resultGeno);
console.log(resultGeno.getGenes().c_str());

j0.get_d().set_x(1.0);
m.validate();

var p1 = m.getPart(1);
console.log(p1.get_p().get_x());

m.makeGeno(resultGeno);
console.log(resultGeno.getGenes().c_str());

console.log("Use delta: " + ((j0.get_usedelta()) ? "True" : "False") );

m.close();

Module.destroy(m);
