console.log("***** update model *****");
/* Init */
var logger = new Module.LoggerToMemory(Module.LoggerBase["Enable"] + Module.LoggerToMemory["StoreAllMessages"]);
var validators = new Module.Validators();
var g1 = new Module.Geno().useValidators(validators);
var modelValidator = new Module.ModelGenoValidator();
validators.append(modelValidator);
Module.destroy(g1);
/* End Init*/

var resultGeno = new Module.Geno();
var testGeno = "//0\np:sh=3,sx=0.05,sy=0.3,sz=0.3\np:sh=3,sx=0.05,sy=0.3,sz=0.3\nj:0,1,sh=1,dx=-0.5,dy=0.35,dz=0.1,ry=-1.0,rx=1.3\n";
var str = new Module.SString();
str.set(testGeno);

var g2 = new Module.Geno(str);
var m = new Module.Model(g2, true);

console.log( "Output after model load." );
m.makeGeno(resultGeno); console.log(resultGeno.getGenes().c_str());
m.validate();           console.log( logger.getMessages().c_str() );
logger.reset();

m.open(); console.log( "m.open()" );
var p2 = new Module.Part();
p2.set_shape(1);
p2.get_p().set_x(1);
p2.get_p().set_y(2);
p2.get_p().set_z(3);
m.addPart(p2);

m.makeGeno(resultGeno);
console.log(resultGeno.getGenes().c_str());

var bad = resultGeno.getGenes().c_str();
bad = bad.substring(0,5) + "d" + bad.substring(5);
var badGenoStr = new Module.SString();
badGenoStr.set(bad);
var badGeno = new Module.Geno(badGenoStr);
//m.setGeno(badGeno);
//m.rebuild();
m.validate();
console.log( logger.getMessages().c_str() );
logger.reset();

m.makeGeno(resultGeno);
console.log(resultGeno.getGenes().c_str());

m.close(); console.log( "m.close()" );

//m.validate();
console.log( logger.getMessages().c_str() );
logger.reset();

resultGeno = new Module.Geno();
m.makeGeno(resultGeno); console.log(resultGeno.getGenes().c_str());

m.open(); console.log( "m.open()" );
var p3 = new Module.Part();
p3.set_shape(2);
p3.get_p().set_x(4);
p3.get_p().set_y(5);
p3.get_p().set_z(6);
m.addPart(p3);

resultGeno = new Module.Geno();
m.makeGeno(resultGeno);  console.log(resultGeno.getGenes().c_str());

m.close();  console.log( "m.close()" );

console.log( logger.getMessages().c_str() );
logger.reset();

Module.destroy(m);
